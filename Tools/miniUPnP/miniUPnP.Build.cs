using System.IO;
using UnrealBuildTool;

public class miniUPnP : ModuleRules
{ 
	public miniUPnP(TargetInfo Target)
	{
		Type = ModuleType.External;
		
		string miniUPnPPath = UEBuildConfiguration.UEThirdPartySourceDirectory + "miniUPnP/";
     
		PublicIncludePaths.Add( miniUPnPPath + "include" );
		PublicSystemIncludePaths.Add( miniUPnPPath + "include" );
		
		PublicLibraryPaths.Add( miniUPnPPath + "lib");
		PublicAdditionalLibraries.Add( miniUPnPPath + "lib/miniupnpc.lib" );
    }
}