#include "CardGame.h"
#include "CGPlayerController.h"


FMonster* ACGPlayerController::GetMonster( int32 Index )
{
	if( Index > -1 && Index < GameState->Monsters.Num() ) return &GameState->Monsters[Index];
	return nullptr;
}

ACGPlayerController::ACGPlayerController( const FObjectInitializer& ObjectInitializer )
	: Super(ObjectInitializer) {
	
	bReplicates = true;
	bAlwaysRelevant = true;
	PrimaryActorTick.bCanEverTick = true;

	GameState = nullptr;
	CPlayerState = nullptr;
}

void ACGPlayerController::SetupInputComponent() {
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("EndPhase", IE_Pressed, this, &ACGPlayerController::cvar_EndPhase);
	InputComponent->BindAction("StartGame", IE_Pressed, this, &ACGPlayerController::StartGame);
}

void ACGPlayerController::cvar_EndPhase() {
	EndPhase();
}

void ACGPlayerController::StartGame()
{
	if( Role < ROLE_Authority ) ServerStartGame();
	else GetWorld()->GetGameState<ACGGameState>()->StartGame( dynamic_cast<ACGPlayerState*>( PlayerState ) );
}
bool ACGPlayerController::ServerStartGame_Validate() { return true; }
void ACGPlayerController::ServerStartGame_Implementation() { StartGame(); }

bool ACGPlayerController::EndPhase()
{
	if( CPlayerState->IsActivePlayer )
	{
		if( Role < ROLE_Authority ) ServerEndPhase();
		else {
			GameState->EndPhase();
		}
		return true;
	}
	return false;
}
bool ACGPlayerController::ServerEndPhase_Validate() {return true;}
void ACGPlayerController::ServerEndPhase_Implementation() {EndPhase();}

bool ACGPlayerController::EndTurn()
{
	if (CPlayerState->IsActivePlayer)
	{
		if (Role < ROLE_Authority) ServerEndTurn();
		else {
			switch (GameState->CurrentPhase)
			{
			case (ECGPhase::CGEquipPhase) :
				GameState->EndPhase();
				GameState->EndPhase();
				GameState->EndPhase();
				break;
			case (ECGPhase::CGAttackPhase) :
				GameState->EndPhase();
				GameState->EndPhase();
				break;
			case (ECGPhase::CGShopPhase) :
				GameState->EndPhase();
				break;
			}
		}
		return true;
	}
	return false;
}
bool ACGPlayerController::ServerEndTurn_Validate() {return true;}
void ACGPlayerController::ServerEndTurn_Implementation() {EndTurn();}

bool ACGPlayerController::DiscardCard( int32 HandIndex )
{
	if( CPlayerState->IsActivePlayer && CPlayerState->CheckAbilityMask( ECGAction::CGDiscard, CPlayerState->HandCards[HandIndex] ) )
	{
		if( Role < ROLE_Authority ) ServerDiscardCard( HandIndex );
		else CPlayerState->RemoveCardFromHand( HandIndex );
		return true;
	}
	return false;
}

bool ACGPlayerController::ServerSelectSlot_Validate( ECGCard::Type SlotType, bool Owning ) { return true; }
void ACGPlayerController::ServerSelectSlot_Implementation( ECGCard::Type SlotType, bool Owning ) { SelectCard( SlotType, Owning ); }
void ACGPlayerController::OwnerSelectSlotResult_Implementation( bool Result ) { SelectCardResult( Result ); }

void ACGPlayerController::SelectCard( ECGCard::Type SlotType, bool Owning )
{
	if( CPlayerState->IsActivePlayer && GameState->CurrentPhase != ECGPhase::CGShopPhase &&
		CPlayerState->CheckAbilityMask( ECGAction::CGSelect, CPlayerState->GetSlot( SlotType ) ) )
	{
		if( Role < ROLE_Authority )
		{
			ServerSelectSlot( SlotType, Owning );
			return;
		}
		if( Owning ) CPlayerState->SelectSlot( SlotType, nullptr );
		else CPlayerState->SelectSlot( SlotType, OPlayerState );
		CPlayerState->PushUIAction( ECGAction::CGSelect );
		OwnerSelectSlotResult( true );
		return;
	}
	OwnerSelectSlotResult( false );
}

bool ACGPlayerController::ServerDiscardCard_Validate( int32 HandIndex ) { return true; }
void ACGPlayerController::ServerDiscardCard_Implementation(int32 HandIndex) { DiscardCard(HandIndex); }

void ACGPlayerController::AttackPlayer( ACGPlayerState* ps )
{
	if( CPlayerState->IsActivePlayer && GameState->CurrentPhase != ECGPhase::CGShopPhase &&
		CPlayerState->CheckAbilityMask( ECGAction::CGAttack, CPlayerState->GetSlot( ECGCard::CGWeapon ) ) )
	{
		if( Role < ROLE_Authority )
		{
			ServerAttackPlayer( ps );
			return;
		}
		int32 Result = CPlayerState->Attack( nullptr, ps );
		if( Result != -1 ) CPlayerState->PushUIAction( ECGAction::CGAttack );
		OwnerAttackPlayerResult( Result );
		return;
	}
	OwnerAttackPlayerResult( -1 );
}
bool ACGPlayerController::ServerAttackPlayer_Validate( ACGPlayerState* ps ) { return true; }
void ACGPlayerController::ServerAttackPlayer_Implementation( ACGPlayerState* ps ) { AttackPlayer( ps ); }
void ACGPlayerController::OwnerAttackPlayerResult_Implementation( int32 Result )
{
	AttackPlayerResult( Result );
}

void ACGPlayerController::AttackMonster( int32 monster)
{
	if( CPlayerState->IsActivePlayer && GameState->CurrentPhase != ECGPhase::CGShopPhase &&
		CPlayerState->CheckAbilityMask( ECGAction::CGAttack, GameState->Monsters[monster].CardInformations ) )
	{
		if( Role < ROLE_Authority ) ServerAttackMonster( monster);
		else
		{
			int32 Result = CPlayerState->Attack( GetMonster( monster ), OPlayerState );
			if( Result != -1 ) CPlayerState->PushUIAction( ECGAction::CGAttack );
			OwnerAttackMonsterResult( Result );
		}
		return;
	}
	OwnerAttackMonsterResult( -1 );
}
bool ACGPlayerController::ServerAttackMonster_Validate( int32 monster) { return true; }
void ACGPlayerController::ServerAttackMonster_Implementation( int32 monster) { AttackMonster( monster); }
void ACGPlayerController::OwnerAttackMonsterResult_Implementation( int32 Result )
{
	AttackMonsterResult( Result );
}

void ACGPlayerController::EquipCard( FCard card )
{
	if( CPlayerState->IsActivePlayer && GameState->CurrentPhase == ECGPhase::CGEquipPhase &&
		CPlayerState->CheckAbilityMask( ECGAction::CGEquip, card ) )
	{
		if( Role < ROLE_Authority )
		{
			ServerEquipCard(card);
			return;
		}
		CPlayerState->EquipCard(card);
		CPlayerState->PushUIAction( ECGAction::CGEquip );
		OwnerEquipCardResult( true );
		return;
	}
	OwnerEquipCardResult( false );
}
bool ACGPlayerController::ServerEquipCard_Validate(FCard card) { return true; }
void ACGPlayerController::ServerEquipCard_Implementation(FCard card) { EquipCard(card); }
void ACGPlayerController::OwnerEquipCardResult_Implementation( bool Result ) { EquipCardResult( Result ); }

bool ACGPlayerController::ClearShopSelection() {
	if (CPlayerState->IsActivePlayer && GameState->CurrentPhase == ECGPhase::CGShopPhase) {
		if (Role < ROLE_Authority) {
			ServerClearShopSelection();
		}
		else {
			CPlayerState->ShopSelectionCards.Empty();
		}
		return true;
	}
	return false;
}
bool ACGPlayerController::ServerClearShopSelection_Validate() { return true; }
void ACGPlayerController::ServerClearShopSelection_Implementation() { ClearShopSelection(); }
void ACGPlayerController::OwnerShopSelectionResult_Implementation( const TArray<FCard>& CardSelection ) { ShopCardSelection( CardSelection ); }

bool ACGPlayerController::AddCardFromSelection(int32 Index) {
	if (CPlayerState->IsActivePlayer && GameState->CurrentPhase == ECGPhase::CGShopPhase) {
		if (Role < ROLE_Authority) {
			ServerAddCardFromSelection(Index);
		}
		else {
			CPlayerState->AddCardToHand(CPlayerState->ShopSelectionCards[Index]);
		}
		return true;
	}
	return false;
}
bool ACGPlayerController::ServerAddCardFromSelection_Validate( int32 Index ) { return true; }
void ACGPlayerController::ServerAddCardFromSelection_Implementation(int32 Index) { AddCardFromSelection(Index); }

void ACGPlayerController::BuyCard( ECGCard::Type Type ) 
{
	if( CPlayerState->IsActivePlayer && Type != ECGCard::CGNone && Type != ECGCard::CGMonster ) 
	{
		if( Role < ROLE_Authority ) ServerBuyCard( Type );
		else
		{
			// Check if Player has enough Gold to afford Card 
			int32 ShopCost = 4; 
			if ( CPlayerState->Gold > ShopCost )
			{
				if( GameState->CurrentPhase == ECGPhase::CGEquipPhase ) EndPhase();
				if( GameState->CurrentPhase == ECGPhase::CGAttackPhase ) EndPhase();

				CPlayerState->Gold -= ShopCost;
				CPlayerState->OnRep_Gold();
				
				CPlayerState->ShopSelectionCards.Empty( 4 );
				CPlayerState->ShopSelectionCards.Add( GameState->GenerateCard( Type, 1 ) );
				CPlayerState->ShopSelectionCards.Add( GameState->GenerateCard( Type, 1 ) );
				CPlayerState->ShopSelectionCards.Add( GameState->GenerateCard( Type, 2 ) );
				CPlayerState->ShopSelectionCards.Add( GameState->GenerateCard( Type, 3 ) );

				// Generate bonus cards
				if( CPlayerState->Equipment.AbilityID == ASINT( ESlotAbilities::GET_ONE_CARD_MORE_BUY ) )
				{
					CPlayerState->ShopSelectionCards.Add( GameState->GenerateCard( Type, FMath::RandRange( 1.0f, 3.0f ) ) );
					CPlayerState->ShopSelectionCards.Add( GameState->GenerateCard( Type, FMath::RandRange( 1.0f, 3.0f ) ) );
				}
				OwnerShopSelectionResult( CPlayerState->ShopSelectionCards );
			}
			else
			{
				UE_LOG(LogClass, Log, TEXT("*** Not enough money to buy a Card ***"));
				OwnerBuyCardResult( -1 );
			}
		}
	}
	else OwnerBuyCardResult( -1 );
}
bool ACGPlayerController::ServerBuyCard_Validate( ECGCard::Type Type ) { return true; }
void ACGPlayerController::ServerBuyCard_Implementation( ECGCard::Type Type ) { BuyCard( Type ); }
void ACGPlayerController::OwnerBuyCardResult_Implementation( int32 Result ) { BuyCardResult( Result ); }

void ACGPlayerController::CastSpell( FCard card, int32 monster )
{
	if( CPlayerState->IsActivePlayer && GameState->CurrentPhase != ECGPhase::CGShopPhase &&
		!card.bIsSlot && CPlayerState->FullAbilityCheck( ECGAction::CGPlay, card, OPlayerState ) )
	{
		if( Role < ROLE_Authority )
		{
			ServerCastSpell( card, monster );
		}
		else if( CPlayerState->CheckActionCost( card ) ) 
		{
			if( CPlayerState->AddAction( card, GetMonster( monster ), OPlayerState ) )
			{
				if( GameState->CurrentPhase == ECGPhase::CGEquipPhase ) GameState->EndPhase();
				CPlayerState->ApplyActionCost( card );
				CPlayerState->PushUIAction( ECGAction::CGPlay );
				OwnerCastSpellResult( 1 );
			}
			else OwnerCastSpellResult( -2 );
		}
		else OwnerCastSpellResult( 0 );
	}
	else OwnerCastSpellResult( -1 );
}
bool ACGPlayerController::ServerCastSpell_Validate(FCard card, int32 monster) { return true; }
void ACGPlayerController::ServerCastSpell_Implementation(FCard card, int32 monster) { CastSpell(card, monster); }
void ACGPlayerController::OwnerCastSpellResult_Implementation( int32 Result ) { CastSpellResult( Result ); }


void ACGPlayerController::ActivateSlot( ECGCard::Type Slot )
{
	if( CPlayerState->IsActivePlayer && GameState->CurrentPhase != ECGPhase::CGShopPhase )
	{
		if( Role < ROLE_Authority ) ServerActivateSlot( Slot );
		else
		{
			if( GameState->CurrentPhase == ECGPhase::CGEquipPhase ) GameState->EndPhase();
			CPlayerState->ActivateSlotAbility( Slot );
		}
	}
}
bool ACGPlayerController::ServerActivateSlot_Validate( ECGCard::Type Slot ) { return true;  }
void ACGPlayerController::ServerActivateSlot_Implementation( ECGCard::Type Slot ) { ActivateSlot( Slot ); }

bool ACGPlayerController::GetIsActivePlayer()
{
	return CPlayerState->IsActivePlayer;
}

void ACGPlayerController::AddLogEntry( bool Owning, ECGAction::Type Action, FCard Source, FCard Target, int32 Result, FCard Loot )
{
	if( Role < ROLE_Authority )
	{
		Server_AddLog( !Owning, Action, Source, Target, Result, Loot );
	}
	else
	{
		((ACGPlayerController*)UGameplayStatics::GetPlayerController( GetWorld(), 1 ))->Client_AddLog( !Owning, Action, Source, Target, Result, Loot );
	}

	GameState->AddLog( Owning, Action, Source, Target, Result, Loot );
}
bool ACGPlayerController::Server_AddLog_Validate( bool Owning, ECGAction::Type Action, FCard Source, FCard Target, int32 Result, FCard Loot ) { return true; }
void ACGPlayerController::Server_AddLog_Implementation( bool Owning, ECGAction::Type Action, FCard Source, FCard Target, int32 Result, FCard Loot )
{
	GameState->AddLog( Owning, Action, Source, Target, Result, Loot );
}
void ACGPlayerController::Client_AddLog_Implementation( bool Owning, ECGAction::Type Action, FCard Source, FCard Target, int32 Result, FCard Loot )
{
	GameState->AddLog( Owning, Action, Source, Target, Result, Loot );
}

// Helper
void ACGPlayerController::AddSlotWeapon( int32 ID ) { CheckAndAdd( ECGCard::CGWeapon, true, ID ); }
void ACGPlayerController::AddWeapon( int32 ID ) { CheckAndAdd( ECGCard::CGWeapon, false, ID ); }
void ACGPlayerController::AddSlotEquipment( int32 ID ) { CheckAndAdd( ECGCard::CGEquipment, true, ID ); }
void ACGPlayerController::AddEquipment( int32 ID ) { CheckAndAdd( ECGCard::CGEquipment, false, ID ); }


void ACGPlayerController::AddGold( int32 Amount ) { ServerAddGold( Amount ); }
bool ACGPlayerController::ServerAddGold_Validate( int32 Amount ) { return true; }
void ACGPlayerController::ServerAddGold_Implementation( int32 Amount )
{
	CPlayerState->Gold += Amount;
	CPlayerState->OnRep_Gold();
}

bool ACGPlayerController::CheckAndAdd_Validate( ECGCard::Type Slot, bool Equip, int32 ID ) { return true; }
void ACGPlayerController::CheckAndAdd_Implementation( ECGCard::Type Slot, bool Equip, int32 ID )
{
	TArray<FCardData>* Data = nullptr;
	switch( Slot )
	{
		case ECGCard::CGEquipment :
			Data = ( Equip ) ? ( &GameState->EquipmentSlotData ) : ( &GameState->EquipmentData );
			break;
		case ECGCard::CGWeapon :
			Data = ( Equip ) ? ( &GameState->WeaponSlotData ) : ( &GameState->WeaponData );
			break;
	}

	if( Data != nullptr && Data->Num() > ID ) AddSpecificCard( Data->operator []( ID ), Slot, ECGRarity::CGGold, Equip );
}

void ACGPlayerController::AddSpecificCard( const FCardData& Data, ECGCard::Type Type, ECGRarity::Type Rarity, bool bSlot )
{
	FCard NewCard;

	NewCard.CardType = Type;
	NewCard.Level = Data.Level;
	NewCard.AbilityID = Data.CardID;
	NewCard.Name = Data.Name;
	NewCard.Discription = Data.GetDescription( Rarity );
	NewCard.AP = Data.GetAP( Rarity );
	NewCard.AbilityID = Data.GetEffectID( Rarity );
	NewCard.Value = Data.Value;
	NewCard.bIsSlot = bSlot;
	NewCard.Param0 = Data.GetParam( Rarity );
	NewCard.Texture = Data.Texture;
	CPlayerState->AddCardToHand( NewCard );
}

// Helper Functions
void ACGPlayerController::NetInit()
{
	GameState = GetWorld() != NULL ? GetWorld()->GetGameState<ACGGameState>() : NULL;
	CPlayerState = dynamic_cast<ACGPlayerState*>( PlayerState );
	OPlayerState = dynamic_cast<ACGPlayerState*>( ( GameState->PlayerArray[0] == PlayerState ) ? ( GameState->PlayerArray[1] ) : ( GameState->PlayerArray[0] ) );
}
