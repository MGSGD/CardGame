#include "CardGame.h"
#include "CGPlayerState.h"

ACGPlayerState::ACGPlayerState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer) {
	bReplicates = true;

	PlayersName = "Player";
	
	IsActivePlayer = false;
	IsListenServer = false;

	NetUpdateFrequency = 10.f;
	CardCount = 0;
	HandMaxSize = 15;
	Gold = 10;

	GameState = nullptr;
}

void ACGPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME( ACGPlayerState, PlayersName );
	DOREPLIFETIME( ACGPlayerState, Gold );
	DOREPLIFETIME( ACGPlayerState, IsActivePlayer );
	DOREPLIFETIME( ACGPlayerState, CardCount );
	DOREPLIFETIME( ACGPlayerState, HandCards );
	DOREPLIFETIME( ACGPlayerState, Weapon );
	DOREPLIFETIME( ACGPlayerState, Equipment );
	DOREPLIFETIME( ACGPlayerState, CurrentMask );
}

bool ACGPlayerState::AddAction( FCard& SourceCard, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget )
{
	bool NonAbilityBlock = ApplyAbilityADD( SourceCard, MonsterTarget, PlayerTarget );
	if( NonAbilityBlock )
	{
		LastActionCard = SourceCard;
		if( SourceCard.Value > 0 )
		{
			GetSlot(SourceCard.CardType).Value += SourceCard.Value;
			SetAbilityFlag( SourceCard.CardType, EAbilityFlags::CanAttack );
		}
		GetSlotAB( SourceCard.CardType ).Add( SourceCard );
		if( SourceCard.AbilityID != -1 ) GetSlot( SourceCard.CardType ).Discription += ", " + SourceCard.Discription;
		UpdateSlot( SourceCard.CardType );
	}
	return NonAbilityBlock;
}

void ACGPlayerState::BeginPlay()
{
	Super::BeginPlay();
	InitHand();
	GameState = dynamic_cast<ACGGameState*>( GetWorld()->GameState );
}

void ACGPlayerState::InitHand() {
	for (int i = 0; i <= HandMaxSize; i++) {
		HandCards.Add(FCard());
	}
}

void ACGPlayerState::AddCardToHand( const FCard& Card) {
	if (CardCount < HandMaxSize ) {
		HandCards[CardCount].CopyFromOtherCard(Card);
		UE_LOG(LogClass, Log, TEXT("*** Added Card at position %d to hand ***"), CardCount);
		OnRep_Hand( CardCount++ );
	}
	else UE_LOG(LogClass, Log, TEXT("*** Hand is full ***"));
}

void ACGPlayerState::RemoveCardFromHand(int32 HandIndex) {
	// Improvement later on
	FCard& Card = HandCards[HandIndex];
	UE_LOG(LogClass, Log, TEXT("*** Removed Card from hand ***"));
	if( !( Card == LastActionCard ) ) ApplyAbilityDISC( Card );
	Card.EmptyCard();
	OrderCards(HandIndex);	
	OnRep_Hand(--CardCount);
}

void ACGPlayerState::OrderCards(int32 Index) {
	for (int i = Index; i < HandMaxSize; i++) {
		HandCards[i] = HandCards[i + 1];
	}
	HandCards[HandMaxSize].EmptyCard();
}

void ACGPlayerState::EquipCard( const FCard& Card ){
	GetSlot( Card.CardType ) = Card;
	// TODO : take care of old slot actions ?
	if( Card.CardType == ECGCard::CGWeapon ) WeaponDefaults = Card;
	else if( Card.CardType == ECGCard::CGEquipment ) EquipmentDefaults = Card;
	UpdateSlot( Card.CardType );
}

int32 ACGPlayerState::Attack( FMonster* Target, ACGPlayerState* Other )
{
	FCard& TargetSlot = GetSlot( ECGCard::CGWeapon );
	int32 Result = -1;
	
	// skip empty attacks
	if( !TargetSlot.IsValidCard() || ( GetSlotAB( ECGCard::CGWeapon ).Num() == 0 && TargetSlot.AbilityID == -1 ) ) return Result;

	ApplyAbilitySLOT( TargetSlot, Target, Other );
	if( HasAbilityFlag( ECGCard::CGWeapon, EAbilityFlags::CanAttack ) )
	{
		if( Target != nullptr ) // attack monster
		{
			Result = Target->RollAttack();
			if( ApplyAbilityMONSTERDMG() || Result <= TargetSlot.Value ) // win fight
			{
				FLoot Loot = Target->SlayMonster();
				ApplyAbilityLOOT( Loot, Target, Other );
				int32 LootGold = Loot.Gold; // reuse
				if( Loot.Card.CardType == ECGCard::CGGold ) LootGold += Loot.Card.Value;
				else if( Loot.Card.IsValidCard() ) AddCardToHand( Loot.Card );
				
				Gold += LootGold;
				OnRep_Gold();
			}
			else // lose fight -> result * -1
			{
				Gold -= min( Gold, Result );
				OnRep_Gold();
				Result *= -1;
			}
		}
		else if( Other != nullptr ) // attack player -> steal gold of value ( calculated by abilities )
		{
			Result = min( TargetSlot.Value, Other->Gold );
			Other->Gold -= Result;
			Gold += Result;
			OnRep_Gold();
			Other->OnRep_Gold();
		}
	}
	if( Result != -1 )
	{
		if( GameState->CurrentPhase == ECGPhase::CGEquipPhase ) GameState->EndPhase();
		ResetSlot( ECGCard::CGWeapon );
	}
	return Result;
}

void ACGPlayerState::PushUIAction( ECGAction::Type Type )
{
	if( Role < ROLE_Authority ) ServerPushUIAction( Type );
	else
	{
		if( CurrentMask.Action == Type && --CurrentMask.Counter < 1 )
		{
			if( CurrentMask.ChainID != -1 )
			{
				CurrentMask = GameState->GetMaskEntry( CurrentMask.ChainID );
			}
			else CurrentMask = FAbilityMask();
			AbilityMaskUpdated();
		}
	}
}

void ACGPlayerState::SelectSlot( ECGCard::Type Type, ACGPlayerState* Other )
{
	if( Type != ECGCard::CGNone )
	{
		if( Other == nullptr ) ApplyAbilitySELF( Type ); // own slot selected
		else ApplyAbilityOPP( Type, Other ); // Opponent slot selected
	}
}


bool ACGPlayerState::FullAbilityCheck( ECGAction::Type Type, const FCard& Card, ACGPlayerState* Other )
{
	if( CurrentMask.Action == ECGAction::CGNone )
	{
		bool PreCheck = false;
		CurrentMask = GameState->GetAbilityMask( Card.AbilityID );
		if( CurrentMask.Action != ECGAction::CGNone )
		{
			bool bFlagLeft = false;
			bool bFlagRight = false;
			bool bMainFlag = false;
			switch( CurrentMask.Action )
			{
				case ECGAction::CGAttack:
					if( Weapon.Value > 0 ) { PreCheck = true; break; }
					if( !HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::Player ) || Other->Gold < 1 )
					{
						for( const FCard& HCard : HandCards )
						{
							if( HCard.bIsSlot &&
								HCard.CardType == ECGCard::CGWeapon &&
								HCard.Value > 0 &&
								Weapon.AP >= HCard.AP )
							{
								PreCheck = true;
								break;
							}
						}
					}
					else PreCheck = true;
					break;
				case ECGAction::CGDiscard:
					bMainFlag = true;
				case ECGAction::CGPlay:
					bFlagLeft = HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::WeaponActions );
					bFlagRight = HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::EquipmentActions );
					for( const FCard& HCard : HandCards )
					{
						if( HCard.bIsSlot ) continue;
						FCard Copy = HCard;
						switch( HCard.CardType )
						{
						case ECGCard::CGEquipment:
							if( bFlagRight && ( bMainFlag || ApplyAbilityCOST( Copy, true ) ) ) PreCheck = true;
							break;
						case ECGCard::CGWeapon:
							if( bFlagLeft && ( bMainFlag || ApplyAbilityCOST( Copy, true ) ) ) PreCheck = true;
							break;
						}
						if( PreCheck ) break;
					}
					break;
				case ECGAction::CGSelect:
					if( ( HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::Weapon ) && Weapon.CardType != ECGCard::CGNone ) ||
						( HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::Equipment ) && Equipment.CardType != ECGCard::CGNone ) ||
						( HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::WeaponOpponent ) && Other->Weapon.CardType != ECGCard::CGNone ) ||
						( HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::EquipmentOpponent ) && Other->Equipment.CardType != ECGCard::CGNone ) )
						PreCheck = true;
					break;
				default:
					PreCheck = true; // just skip for not implemented actions
					break;
			}
		}
		else return true;
		CurrentMask = FAbilityMask();
		return PreCheck;
	}
	else return CheckAbilityMask( Type, Card );
}

bool ACGPlayerState::ServerPushUIAction_Validate( ECGAction::Type Type ) { return true; }
void ACGPlayerState::ServerPushUIAction_Implementation( ECGAction::Type Type ) { PushUIAction( Type ); }

bool ACGPlayerState::CheckAbilityMask( ECGAction::Type Type, const FCard& Card )
{
	if( CurrentMask.Action == ECGAction::CGNone ) return true;
	if( CurrentMask.Action != Type ) return false;

	// Player Attack
	if( Card.bIsSlot && HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::Player ) ) return true;
	
	switch( Card.CardType )
	{
		case ECGCard::CGMonster :
			return HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::Monser );
			break;

		case ECGCard::CGWeapon :
			if( Card.bIsSlot )
			{
				if( !HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::Weapon ) ) return false;
			}
			else if( !HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::WeaponActions ) ) return false;
			return true;
			break;

		case ECGCard::CGEquipment :
			if( Card.bIsSlot )
			{
				if( !HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::Equipment ) ) return false;
			}
			else if( !HAS_FLAG( CurrentMask.TargetFlag, EAbbilityTargets::EquipmentActions ) ) return false;
			return true;
			break;
	}

	return false;
}

bool ACGPlayerState::CheckActionCost( FCard& CardInfo )
{
	ApplyAbilityCOST( CardInfo );
	return GetSlot( CardInfo.CardType ).AP >= CardInfo.AP;
}

void ACGPlayerState::ApplyActionCost( FCard& CardInfo )
{
	if( CardInfo.AP > 0 )
	{
		GetSlot( CardInfo.CardType ).AP -= CardInfo.AP;
		UpdateSlot( CardInfo.CardType );
	}
}

bool ACGPlayerState::ActivateSlotAbility( ECGCard::Type SlotType )
{
	if( HasAbilityFlag( SlotType, EAbilityFlags::NotActivateable ) ) return false;
	switch( SlotType )
	{
	case ECGCard::CGWeapon :
		if( Weapon.IsValidCard() && Weapon.CanBeActivated() )
		{
			SetAbilityFlag( ECGCard::CGWeapon, EAbilityFlags::UseSlotAbility );
			COM_SlotActived( ECGCard::CGWeapon );
			//OnRep_Weapon(); //need later
			return true;
		}
		break;
	case ECGCard::CGEquipment:
		if( Equipment.IsValidCard() && Equipment.CanBeActivated() )
		{
			SetAbilityFlag( ECGCard::CGEquipment, EAbilityFlags::UseSlotAbility );
			COM_SlotActived( ECGCard::CGEquipment );
			//OnRep_Equipment(); //need later
			return true;
		}
		break;
	}
	return false;
}

void ACGPlayerState::Reset()
{
	if( Weapon.IsValidCard() ) ResetSlot( ECGCard::CGWeapon, false );
	if( Equipment.IsValidCard() ) ResetSlot( ECGCard::CGEquipment, false  );
}

void ACGPlayerState::ResetSlot( ECGCard::Type Type, bool Soft )
{
	int32 Holder = 0;
	switch( Type )
	{
		case ECGCard::CGWeapon :
			if( Soft ) Holder = Weapon.AP;
			Weapon = WeaponDefaults;
			WeaponAbilities.Empty();
			if( Soft ) Weapon.AP = Holder;
			WeaponFlag = EAbilityFlags::None;
			OnRep_Weapon();
			break;
		case ECGCard::CGEquipment :
			if( Soft ) Holder = Equipment.AP;
			Equipment = EquipmentDefaults;
			EquipmentAbilities.Empty();
			if( Soft ) Equipment.AP = Holder;
			EquipmentFlag = EAbilityFlags::None;
			OnRep_Equipment();
			break;
	}
	AbbilityCounter00 = 0;
	AbbilityCounter01 = 0;
}

bool ACGPlayerState::SlotContainID( ECGCard::Type Type, int32 ID )
{
	for( const FCard& Card : GetSlotAB( Type ) )
	{
		if( Card.AbilityID == ID ) return true;
	}
	return false;
}

FCard& ACGPlayerState::GetCardFromSlot( ECGCard::Type Type, int32 ID )
{
	for( FCard& Card : GetSlotAB( Type ) )
	{
		if( Card.AbilityID == ID ) return Card;
	}
	return GetSlot( Type );
}

void ACGPlayerState::RemoveIDFromSlot( ECGCard::Type Type, int32 ID )
{
	FCard* Element = nullptr;
	for( FCard& Card : GetSlotAB( Type ) )
	{
		if( Card.AbilityID == ID )
		{
			if( Card.Value > 0 ) Element = &Card;
			else Card.AbilityID = -1;
			break;
		}
	}
	if( Element != nullptr )
	{
		GetSlotAB( Type ).RemoveSingle( *Element );
	}
}

int32 ACGPlayerState::CountIDInSlot( int32 ID, ECGCard::Type Slot )
{
	int32 Result = 0;
	for( const FCard& CurrentCard : GetSlotAB( Slot ) )
	{
		if( CurrentCard.AbilityID == ID ) Result++;
	}
	return Result;
}

bool ACGPlayerState::ApplyAbilityTURNEND( ACGPlayerState* OtherPlayer )
{
	// Equipment
	switch( Equipment.AbilityID )
	{
		case ESlotAbilities::GOLD_AT_TURN_END :
			Gold += Equipment.Param0;
			OnRep_Gold();
			break;

		case ESlotAbilities::GOLD_FOR_AP_TURN_END :
			Gold += Equipment.AP * Equipment.Param0 + Weapon.AP * Equipment.Param0;
			OnRep_Gold();
			break;
	}

	switch( OtherPlayer->Equipment.AbilityID )
	{
		case ESlotAbilities::GOLD_AT_TURN_END :
			OtherPlayer->Gold += OtherPlayer->Equipment.Param0;
			OtherPlayer->OnRep_Gold();
			break;
	}

	return false;
}

bool ACGPlayerState::ApplyAbilityTURNSTART( ACGPlayerState* PlayerTarget )
{
	

	return false;
}

bool ACGPlayerState::ApplyAbilityCOST( FCard& Source, bool bOnlyCheck /*= false */ )
{
	switch( Weapon.AbilityID )
	{
		case ESlotAbilities::REDUCE_ACTION_AP_COST :
			if( Source.AP > 1 )
			{
				Source.AP -= Weapon.Param0;
				if( Source.AP < 1 ) Source.AP = 1;
			}
			break;

		case ESlotAbilities::REDUCE_AP_INCREASE_DMG:
			if( Source.CardType == ECGCard::CGWeapon )
			{
				if( Source.AP > Weapon.Param0 ) Source.AP -= Weapon.Param0;
				else Source.AP = 0;
				if( !bOnlyCheck ) Source.Value += Weapon.Param0 + 0.7f * Weapon.Param0; // increase dmg slightly
			}
			break;
	}

	if( Source.AP > 0 && SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::PLAY_CARD_FREE ) ) )
	{
		if( !bOnlyCheck ) RemoveIDFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::PLAY_CARD_FREE ) );
		Source.AP = 0;
	}
	if( Source.AP > 0 && SlotContainID( ECGCard::CGWeapon, ASINT( EActionAbilities::PLAY_SPELL_FREE ) ) )
	{
		if( !bOnlyCheck ) RemoveIDFromSlot( ECGCard::CGWeapon, ASINT( EActionAbilities::PLAY_SPELL_FREE ) );
		Source.AP = 0;
	}
	if( Source.AP > 0 && SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::DISCARD_FOR_FREE_PLAY_ONCE ) ) && AbbilityCounter01 == 1 )
	{
		if( !bOnlyCheck ) RemoveIDFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::DISCARD_FOR_FREE_PLAY_ONCE ) );
		Source.AP = 0;
	}

	if( Source.AP > 1 && SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::REDUCE_ACTION_AP_COST ) ) )
	{
		Source.AP -= GetCardFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::REDUCE_ACTION_AP_COST ) ).Param0;
		if( Source.AP < 1 ) Source.AP = 1;
	}

	return Source.AP <= GetSlot( Source.CardType ).AP;
}

bool ACGPlayerState::ApplyAbilityADD( FCard& Source, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget )
{
	bool bWeaponAbilityActive = HasAbilityFlag( ECGCard::CGWeapon, EAbilityFlags::UseSlotAbility );
	bool bEquipmentAbilityActive = HasAbilityFlag( ECGCard::CGEquipment, EAbilityFlags::UseSlotAbility );
	bool bAttackMonster = ( MonsterTarget != nullptr );
	bool bAttackPlayer = ( PlayerTarget != nullptr );

	// Weapons
	//
	switch( Weapon.AbilityID )
	{
		case ESlotAbilities::INCREASE_EACH_ATTACK_BY_ONE:
			if( Source.Value > 0 ) Source.Value += 1;
			break;

		case ESlotAbilities::PLAY_ATTACK_CARD_TWICE:
			if( bWeaponAbilityActive && Source.CardType == ECGCard::CGWeapon )
			{
				RemoveAbilityFlag( ECGCard::CGWeapon, EAbilityFlags::UseSlotAbility );
				EAddAction( Source, MonsterTarget, PlayerTarget );
			}
			break;
	}

	// Equipment
	//
	switch( Equipment.AbilityID )
	{
		case ESlotAbilities::GET_GOLD_EACH_ATTACK_CARD :
			if( Source.Value > 0 )
			{
				Gold += Equipment.Param0;
				OnRep_Gold();
			}
			break;
	}

	// Both
	/*
	for( ECGCard::Type i : { ECGCard::CGWeapon, ECGCard::CGEquipment } )
	{
		switch( GetSlot( i ).AbilityID )
		{




		}
	}
	*/

	// Check for PLAY_ACTION_CARD_TWICE
	if( SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::PLAY_ACTION_CARD_TWICE ) )  )
	{
		RemoveIDFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::PLAY_ACTION_CARD_TWICE ) );
		AddCardToHand( Source );
	}

	if( SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::GOLD_EACH_CARD_ON_ATTACK ) ) )
	{
		Gold += GetCardFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::GOLD_EACH_CARD_ON_ATTACK ) ).Param0;
		OnRep_Gold();
	}

	// Actions
	//
	if( Source.AbilityID == -1 ) return true;
	switch( Source.AbilityID )
	{
		case EActionAbilities::DRAW_TWO_ATTACK_CARDS :
		{
			FCard NewActionCard = GameState->GenerateCard( ECGCard::CGWeapon, 1 );
			NewActionCard.AP = 0;
			EAddAction( NewActionCard, MonsterTarget, PlayerTarget );
			NewActionCard = GameState->GenerateCard( ECGCard::CGWeapon, 1 );
			NewActionCard.AP = 0;
			EAddAction( NewActionCard, MonsterTarget, PlayerTarget );
		}	break;

		case EActionAbilities::DMG_FOREACH_HAND_CARD :
			GetSlot( Source.CardType ).Value += Source.Param0 * HandCards.Num();
			break;

		case EActionAbilities::DESTROY_OPPONENT_ACTION_CARD :
			for( int32 i = 0; i < Source.Param0; ++i ) // 
			{
				int32 RandomIndex = FMath::RandRange( 0, PlayerTarget->HandCards.Num() - 1 );
				if( PlayerTarget->HandCards[RandomIndex].bIsSlot ) // if random failed - get next valid action card
				{
					for( int32 j = 0; j < PlayerTarget->HandCards.Num(); ++j )
					{
						if( !PlayerTarget->HandCards[j].bIsSlot )
						{
							PlayerTarget->RemoveCardFromHand( j );
							break;
						}
					}
				}
				else PlayerTarget->RemoveCardFromHand( RandomIndex );
			}
			break;

		case EActionAbilities::INCREASE_WEAPON_AP :
			SetAbilityFlag( ECGCard::CGWeapon, EAbilityFlags::OnlyMonsterTarget ); // check for onlyplayer targeting ?
			Weapon.AP += 1;
			break;
	}

	if( ExistingCard ) // set ability mask only for existing card ( not those created internal by efffects )
	{
		CurrentMask = GameState->GetAbilityMask( Source.AbilityID );
		AbilityMaskUpdated();
	}

	return true;
}

int32 ACGPlayerState::ApplyAbilitySELF( ECGCard::Type Type )
{
	int32 Result = -1;

	if( SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::INCREASE_AP_UNTIL_TURN_END ) ) )
	{
		GetSlot( Type ).AP += GetCardFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::INCREASE_AP_UNTIL_TURN_END ) ).Param0;
		RemoveIDFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::INCREASE_AP_UNTIL_TURN_END ) );
		Result = 0;
	}

	return Result;
}

int32 ACGPlayerState::ApplyAbilityOPP( ECGCard::Type Type, ACGPlayerState* Other )
{
	int32 Result = -1;
	// Check for DUPLICATE_OPPONENT_SLOT_ITEM
	if( SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::DUPLICATE_OPPONENT_SLOT_ITEM ) ) )
	{
		EquipCard( Other->GetSlot( Type ) );
		Result = 0;
	}

	return Result;
}

bool ACGPlayerState::ApplyAbilitySLOT( FCard& Source, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget )
{
	switch( Equipment.AbilityID )
	{
		case ESlotAbilities::GOLD_FOR_ACTION_OPPONENT:
			if( MonsterTarget == nullptr && PlayerTarget != nullptr )
			{
				Gold += GetSlotAB( Source.CardType ).Num() * Equipment.Param0;
				OnRep_Gold();
			}
			break;
	}

	if( Weapon.AbilityID == ASINT( ESlotAbilities::HIT_ADDITIONAL_MONSTER ) )
	{
		int32 RandomIndex = FMath::FRandRange( 0, GameState->Monsters.Num() - 1 );
		FMonster* RandomTarget = &GameState->Monsters[RandomIndex];
		if( RandomTarget == MonsterTarget )
		{
			if( RandomIndex + 1 == GameState->Monsters.Num() ) RandomTarget = &GameState->Monsters[--RandomIndex];
			else RandomTarget = &GameState->Monsters[++RandomIndex];
		}
		if( RandomTarget->RollAttack() <= Source.Value * ( Weapon.Param0 / 100 ) )
		{
			FLoot Loot = RandomTarget->SlayMonster();
			ApplyAbilityLOOT( Loot, MonsterTarget, PlayerTarget );
			int32 EarnedGold = Loot.Gold;
			if( Loot.Card.CardType == ECGCard::CGGold ) EarnedGold += Loot.Card.Value;
			else if( Loot.Card.IsValidCard() ) AddCardToHand( Loot.Card );

			Gold += EarnedGold;
			OnRep_Gold();
		}
	} 
	else if( Weapon.AbilityID == ASINT( ESlotAbilities::HIT_TWO_ADD_MONSTER ) )
	{
		FMonster* Old = nullptr;
		for( int32 i = 0; i < 1; ++i )
		{
			int32 RandomIndex = FMath::FRandRange( 0, GameState->Monsters.Num() - 1 );
			FMonster* RandomTarget = &GameState->Monsters[RandomIndex];
			if( RandomTarget == MonsterTarget || RandomTarget == Old )
			{
				if( RandomIndex + 1 == GameState->Monsters.Num() ) RandomTarget = &GameState->Monsters[--RandomIndex];
				else RandomTarget = &GameState->Monsters[++RandomIndex];
			}
			Old = RandomTarget;
			if( RandomTarget->RollAttack() <= Source.Value * ( Weapon.Param0 / 100 ) )
			{
				FLoot Loot = RandomTarget->SlayMonster();
				ApplyAbilityLOOT( Loot, MonsterTarget, PlayerTarget );
				int32 EarnedGold = Loot.Gold;
				if( Loot.Card.CardType == ECGCard::CGGold ) EarnedGold += Loot.Card.Value;
				else if( Loot.Card.IsValidCard() ) AddCardToHand( Loot.Card );

				Gold += EarnedGold;
				OnRep_Gold();
			}
		}
	}

	// custom
	if( HasAbilityFlag( ECGCard::CGWeapon, EAbilityFlags::CanAttack ) )
	{
		if( SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::INCREASE_AP_FOREACH_CARD ) ) )
		{
			RemoveIDFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::INCREASE_AP_FOREACH_CARD ) );
			Source.AP += GetCardFromSlot( ECGCard::CGEquipment, ASINT( EActionAbilities::INCREASE_AP_FOREACH_CARD ) ).Param0 * EquipmentAbilities.Num();
		}
	}
	

	return false;
}

bool ACGPlayerState::ApplyAbilityMONSTERDMG()
{
	

	return false;
}

bool ACGPlayerState::ApplyAbilityLOOT( FLoot& Source, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget )
{
	// Equipment
	//
	switch( Equipment.AbilityID )
	{
		case ESlotAbilities::ALL_MONSTER_GIVE_GOLD :
			Gold += Equipment.Param0;
			OnRep_Gold();
			break;

		case ESlotAbilities::MULTIPLY_GOLD_DROP :
			Source.Gold *= Equipment.Param0;
			break;
	}

	switch( PlayerTarget->Equipment.AbilityID )
	{
		case ESlotAbilities::ALL_MONSTER_GIVE_GOLD :
			PlayerTarget->Gold += PlayerTarget->Equipment.Param0;
			PlayerTarget->OnRep_Gold();
			break;

	}

	// Weapon
	//
	switch( Weapon.AbilityID )
	{
		case ESlotAbilities::INCREASE_AP_FOR_MONSTER :
			if( Weapon.Param0 > 0 && Equipment.IsValidCard() )
			{
				Equipment.AP++;
				EquipmentDefaults.AP++;
			}
			Weapon.AP++;
			WeaponDefaults.AP++;
			break;	
	}

	// custom stuff

	// GET_PART_OF_OPPONENT_GOLD_LOOT
	if( ASSLOT( PlayerTarget->Equipment.AbilityID ) == ESlotAbilities::GET_PART_OF_OPPONENT_GOLD_LOOT &&
		!PlayerTarget->HasAbilityFlag( ECGCard::CGEquipment, EAbilityFlags::UseSlotAbility ) )
	{
		PlayerTarget->SetAbilityFlag( ECGCard::CGEquipment, EAbilityFlags::UseSlotAbility );
		int32 StolenGold = Source.Gold / 2;
		Source.Gold -= StolenGold;
		if( Source.Card.CardType == ECGCard::CGGold )
		{
			int32 BonusGold = Source.Card.Value * ( PlayerTarget->Equipment.Param0 / 100 );
			Source.Card.Value -= BonusGold;
			StolenGold += BonusGold;
		}
		PlayerTarget->Gold += StolenGold;
		PlayerTarget->OnRep_Gold();
		return true;
	}

	return false;
}

bool ACGPlayerState::ApplyAbilityDISC( FCard& Source )
{
	// !Beware of Slots that are distroyed!

	switch( Weapon.AbilityID )
	{
		case ESlotAbilities::DISCARD_ACTION_REFILL_SLOT_AP :
			if( !Source.bIsSlot && ++AbbilityCounter00 >= Weapon.Param0 )
			{
				Weapon.AP = min( Weapon.AP + 2, WeaponDefaults.AP );
				AbbilityCounter00 -= Weapon.Param0;
				OnRep_Weapon();
			}
			break;
	}

	if( !Source.bIsSlot && SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::DISCARD_FOR_FREE_PLAY_ONCE ) ) )
	{
		AbbilityCounter01 = 1;
	}

	if( !Source.bIsSlot && SlotContainID( ECGCard::CGEquipment, ASINT( EActionAbilities::DISCARD_FOR_NEW_ONE ) ) )
	{
		AddCardToHand( GameState->GenerateCard( Source.CardType, Source.Level ) );
	}

	if( !Source.bIsSlot && SlotContainID( ECGCard::CGWeapon, ASINT( EActionAbilities::DISCARD_FOR_DMG ) ) )
	{
		Weapon.Value++;
		OnRep_Weapon();
	}

	return false;
}

bool ACGPlayerState::HasAbilityFlag( ECGCard::Type Type, EAbilityFlags flag ) const
{
	switch( Type )
	{
	case ECGCard::CGWeapon:
		return ( WeaponFlag & flag ) == flag;
	case ECGCard::CGEquipment:
		return ( EquipmentFlag & flag ) == flag;
	}
	return false;
}

void ACGPlayerState::RemoveAbilityFlag( ECGCard::Type Type, EAbilityFlags flag )
{
	switch( Type )
	{
		case ECGCard::CGWeapon :
			WeaponFlag = WeaponFlag & ~flag;
			break;
		case ECGCard::CGEquipment:
			EquipmentFlag = EquipmentFlag & ~flag;
			break;
	}
}

void ACGPlayerState::SetAbilityFlag( ECGCard::Type Type, EAbilityFlags flag )
{
	switch( Type )
	{
		case ECGCard::CGWeapon :
			WeaponFlag |= flag;
			break;
		case ECGCard::CGEquipment :
			EquipmentFlag |= flag;
			break;
	}
}

FCard& ACGPlayerState::GetSlot( ECGCard::Type Type )
{
	switch( Type )
	{
		case ECGCard::CGWeapon : return Weapon;
		case ECGCard::CGEquipment : return Equipment;
	}
	return Equipment; // change later
}

void ACGPlayerState::COM_SlotActived_Implementation( ECGCard::Type Slot ) {	SlotActivated( Slot ); }

TArray<FCard>& ACGPlayerState::GetSlotAB( ECGCard::Type Type )
{
	switch( Type )
	{
	case ECGCard::CGWeapon:
		return WeaponAbilities;
	case ECGCard::CGEquipment:
		return EquipmentAbilities;
	}
	return EquipmentAbilities;
}

void ACGPlayerState::UpdateSlot( ECGCard::Type Type )
{
	switch( Type )
	{
	case ECGCard::CGWeapon:
		OnRep_Weapon();
		break;
	case ECGCard::CGEquipment:
		OnRep_Equipment();
		break;
	}
}
