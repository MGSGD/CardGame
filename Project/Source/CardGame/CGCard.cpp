// Fill out your copyright notice in the Description page of Project Settings.

#include "CardGame.h"
#include "CGCard.h"


// Sets default values
ACGCard::ACGCard() 
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	Owning = false;
	Index = 0;
}

void ACGCard::UpdateCard( const FCard& Informations )
{
	UpdateUI( Informations );
}

bool ACGCard::IsValid( const FCard& Information ) const
{
	return( Information.CardType != ECGCard::CGNone );
}
