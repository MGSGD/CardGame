#pragma once

#include "Engine.h"

#include "Net/UnrealNetwork.h"

#define max(a,b) (((a) > (b)) ? (a) : (b))
#define min(a,b) (((a) < (b)) ? (a) : (b))

#include "CGAbilityEnums.h"

#include "CGMonster.h"
#include "CGPlayerState.h"
#include "CGPlayerController.h"
#include "CGMonster.h"
#include "CGGameState.h"
#include "CGGameInstance.h"
