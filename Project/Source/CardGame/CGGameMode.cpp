// Fill out your copyright notice in the Description page of Project Settings.

#include "CardGame.h"
#include "CGPlayerController.h"
#include "CGGameState.h"
#include "CGPlayerState.h"
#include "CGGameMode.h"

ACGGameMode::ACGGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {
	// use our custom PlayerController class
	PlayerControllerClass = ACGPlayerController::StaticClass();
	GameStateClass = ACGGameState::StaticClass();
	PlayerStateClass = ACGPlayerState::StaticClass();
}
