#include "CardGame.h"
#include "UPNP.h"

#define SEARCH_REQUEST_STRING TEXT("M-SEARCH * HTTP/1.1\r\nHOST:239.255.255.250:1900\r\nMan:\"ssdp:discover\"\r\nST:ssdp:all\r\nMX:2\r\n\r\n")
#define SEARCH_SERVICE_TYPE_0 "WANIPConnection"
#define SEARCH_SERVICE_TYPE_1 "WANPPPConnection"
#define SERACH_DEVICE_TYPE_0 "InternetGatewayDevice"
#define SERACH_DEVICE_TYPE_1 "fritzbox"
#define HTTP_GET_TEMPLATE TEXT("GET %s HTTP/1.1\r\nHost: %s:%d\r\n\r\n")

#define HTTP_SOAP_HEADER TEXT("POST %s HTTP/1.1\r\nHOST: %s:%d\r\nSOAPACTION:\"urn:schemas-upnp-org:service:%s:1#AddPortMapping\"\r\nCONTENT-TYPE: text/xml; charset=\"utf-8\"\r\nContent-Length: %d\r\n\r\n%s")
#define XML_APP_PORT_MAPPING TEXT("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\r\n<s:Body>\r\n<u:AddPortMapping xmlns:u=\"urn:schemas-upnp-org:service:%s:1\">\r\n%s</u:AddPortMapping>\r\n</s:Body>\r\n</s:Envelope>\r\n")
#define PARAM_APP_PORT_MAPPING TEXT("<NewRemoteHost></NewRemoteHost>\r\n<NewExternalPort>%d</NewExternalPort>\r\n<NewProtocol>%s</NewProtocol>\r\n<NewInternalPort>%d</NewInternalPort>\r\n<NewInternalClient>%s</NewInternalClient>\r\n<NewEnabled>1</NewEnabled>\r\n<NewPortMappingDescription>PortMapping</NewPortMappingDescription>\r\n<NewLeaseDuration>86400</NewLeaseDuration>\r\n")

void UUPNP::CloseSock()
{
	if( Sock_Ptr != nullptr )
	{
		Sock_Ptr->Close();
		ISocketSubsystem::Get( PLATFORM_SOCKETSUBSYSTEM )->DestroySocket( Sock_Ptr );
		Sock_Ptr = nullptr;
	}
}

bool UUPNP::ForwardPort( const int32 Port /*= 7777*/, const float CheckIntervall /*= 0.3f*/, const float FullDuration /*= 4.f */ )
{
	if( Discover( CommunicationPort ) )
	{
		RequestedPort = Port;
		Intervall = CheckIntervall;
		Duration = FullDuration / 2.f;

		GetWorld()->GetTimerManager().SetTimer( Complete_Handle, this, &UUPNP::CompleteDiscovery, Duration, false );
		GetWorld()->GetTimerManager().SetTimer( Checker_Handler, this, &UUPNP::CheckDeviceAdvertise, Intervall, true, 0.1f );
		
		return true;
	}
	else UE_LOG( LogTemp, Log, TEXT( "FAILED : UPNP discovery" ) );

	return false;
}

void UUPNP::CheckDeviceAdvertise()
{
	uint32 DataSize;
	while( Sock_Ptr->HasPendingData( DataSize ) )
	{
		TSharedPtr<FInternetAddr> TargetAddress = ISocketSubsystem::Get( PLATFORM_SOCKETSUBSYSTEM )->CreateInternetAddr();
		FArrayReader DataReader;
		DataReader.SetNumUninitialized( DataSize );

		int32 BytesRead = 0;
		while( Sock_Ptr->RecvFrom( DataReader.GetData(), DataReader.Num(), BytesRead, *TargetAddress ) )
		{
			UE_LOG( LogTemp, Log, TEXT( "Recived %d Bytes - %d read" ), DataSize, BytesRead );
			DataSize -= BytesRead;
			if( FindStrInArray( DataReader, TEXT( "200 OK" ), BytesRead ) > 0 ) // Answer found
			{
				DeviceData DEntry;

				int32 URLStart = FindStrInArray( DataReader, TEXT( "http://" ), BytesRead ) + 7; // Check for upnp address
				if( URLStart < 0 ) return;
				int32 URLEnd = FindStrInArray( DataReader, TEXT( "\r" ), BytesRead, URLStart );
				if( URLEnd < 0 ) return;
				int32 PathStart = FindStrInArray( DataReader, TEXT( "/" ), URLEnd, URLStart );
				if( PathStart < 0 ) return;
				if( !CopyArrayToStr( DataReader, DEntry.InfoPath, URLEnd, PathStart ) ) return;

				int32 PortStart = FindStrInArray( DataReader, TEXT( ":" ), PathStart, URLStart ) + 1;
				if( PortStart < 0 ) TargetAddress->GetPort( DEntry.Port );
				else
				{
					FString PortString;
					if( !CopyArrayToStr( DataReader, PortString, PathStart, PortStart ) ) return;
					DEntry.Port = FCString::Atoi( *PortString );
				}

				TargetAddress->GetIp( DEntry.Address );

				if( DeviceList.Num() == DeviceList.AddUnique( DEntry ) )
				{
					UE_LOG( LogTemp, Log, TEXT( "UPNP Device found! - %s" ), *DEntry.InfoPath );
				}
			}
		}
	}
}

void UUPNP::CompleteDiscovery()
{
	GetWorld()->GetTimerManager().ClearTimer( Checker_Handler );
	GetWorld()->GetTimerManager().ClearTimer( Complete_Handle );

	UE_LOG( LogTemp, Log, TEXT( "UPNP Discovery complete" ) );
	CloseSock();

	if( DeviceList.Num() > 0 )
	{
		GetWorld()->GetTimerManager().SetTimer( Checker_Handler, this, &UUPNP::CheckDeviceService, Intervall, true );
		GetWorld()->GetTimerManager().SetTimer( Complete_Handle, this, &UUPNP::CompleteAnalyse, Duration, false );
		UE_LOG( LogTemp, Log, TEXT( "UPNP Analyse Devices started - Found %d" ), DeviceList.Num() );
	}
	else
	{
		UE_LOG( LogTemp, Log, TEXT( "UPNP supported device found!" ) );
		ForwardPortResult.ExecuteIfBound( false );
	}
}

void UUPNP::CheckDeviceService()
{
	if( CurrentDevice < DeviceList.Num() )
	{
		DeviceData& DeviceData = DeviceList[CurrentDevice++];

		FString XMLFile = RequestXMLFile( DeviceData );
		if( XMLFile.IsEmpty() )
		{
			UE_LOG( LogTemp, Log, TEXT( "FAILED: load xml file" ) );
			return;
		}

		FXmlFile DeviceDescription( XMLFile, EConstructMethod::ConstructFromBuffer );
		if( !DeviceDescription.IsValid() )
		{
			UE_LOG( LogTemp, Log, TEXT( "FAILED: Parse XML File (recived) from Device(%s)" ), *DeviceData.InfoPath );
			return;
		}

		UE_LOG( LogTemp, Log, TEXT( "SUCCESS: Read-Description of Device(%s)" ), *DeviceData.InfoPath );
		FXmlNode* DeviceNode = DeviceDescription.GetRootNode()->FindChildNode( TEXT( "device" ) );
		if( DeviceNode == nullptr )
		{
			UE_LOG( LogTemp, Log, TEXT( "FAILED: find 'device' node in device description (%s)" ), *DeviceData.InfoPath );
			return;
		}
		FString DeviceType = DeviceNode->FindChildNode( TEXT( "deviceType" ) )->GetContent();
		if( DeviceType.Find( SERACH_DEVICE_TYPE_1 ) != INDEX_NONE ) // specific old fritzbox data
		{
			DeviceData.Service = DeviceData::WANIP;
			DeviceData.ControlPath = "/upnp/control/WANIPConn1";
			return;
		}
		else if( DeviceType.Find( SERACH_DEVICE_TYPE_0 ) == INDEX_NONE ) return; // Skip on wrong device type

		DeviceNode = DeviceNode->FindChildNode( TEXT( "deviceList" ) );
		if( DeviceNode == nullptr )
		{
			UE_LOG( LogTemp, Log, TEXT( "FAILED: find 'device->deviceList' in device description (%s)" ), *DeviceData.InfoPath );
			return;
		}

		DeviceNode = DeviceNode->FindChildNode( TEXT( "device" ) );
		if( DeviceNode == nullptr )
		{
			UE_LOG( LogTemp, Log, TEXT( "FAILED: find 'device->deviceList->device' node in device description (%s)" ), *DeviceData.InfoPath );
			return;
		}

		DeviceNode = DeviceNode->FindChildNode( TEXT( "deviceList" ) );
		if( DeviceNode == nullptr )
		{
			UE_LOG( LogTemp, Log, TEXT( "FAILED: find 'device->deviceList->device->deviceList' node in device description (%s)" ), *DeviceData.InfoPath );
			return;
		}

		DeviceNode = DeviceNode->FindChildNode( TEXT( "device" ) );
		if( DeviceNode == nullptr )
		{
			UE_LOG( LogTemp, Log, TEXT( "FAILED: find 'device->deviceList->device->deviceList->device' node in device description (%s)" ), *DeviceData.InfoPath );
			return;
		}

		DeviceNode = DeviceNode->FindChildNode( TEXT( "serviceList" ) );
		if( DeviceNode == nullptr )
		{
			UE_LOG( LogTemp, Log, TEXT( "FAILED: find 'device->deviceList->device->deviceList->device->serviceList' node in device description (%s)" ), *DeviceData.InfoPath );
			return;
		}

		for( FXmlNode* ServiceNode : DeviceNode->GetChildrenNodes() )
		{
			if( ServiceNode != nullptr )
			{
				FXmlNode* InfoNode = ServiceNode->FindChildNode( TEXT( "serviceType" ) );
				if( InfoNode != nullptr )
				{
					int32 ForwardServerFound = InfoNode->GetContent().Find( SEARCH_SERVICE_TYPE_0 );
					if( ForwardServerFound == INDEX_NONE )
					{
						ForwardServerFound = InfoNode->GetContent().Find( SEARCH_SERVICE_TYPE_1 );
						if( ForwardServerFound != INDEX_NONE ) DeviceData.Service = DeviceData::WANPP;
					}
					else DeviceData.Service = DeviceData::WANIP;

					if( DeviceData.Service != DeviceData::NONE )
					{
						InfoNode = ServiceNode->FindChildNode( TEXT( "controlURL" ) );
						if( InfoNode != nullptr )
						{
							DeviceData.ControlPath = InfoNode->GetContent();
							UE_LOG( LogTemp, Log, TEXT( "UPNP PortForwarding Device-Service found (%s)" ), *DeviceData.InfoPath );
							break; // Right Service found!
						}
					}
				}
			}
			// Add Debug info for not useable devices/services
		}
	}
}

void UUPNP::CompleteAnalyse()
{
	GetWorld()->GetTimerManager().ClearTimer( Checker_Handler );
	GetWorld()->GetTimerManager().ClearTimer( Complete_Handle );

	UE_LOG( LogTemp, Log, TEXT( "UPNP Read Description completed" ) );
	bool bPortForward = false;
	for( DeviceData& DEntry : DeviceList )
	{
		if( DEntry.ControlPath.IsEmpty() ) continue; // skip on not have the right service
		if( OpenPort( DEntry ) )
		{
			UE_LOG( LogTemp, Log, TEXT( "UPNP PortForward succeeded on Device(%s)" ), *DEntry.ControlPath );
			bPortForward = true;
		}
	}
	if( !bPortForward )
	{
		UE_LOG( LogTemp, Log, TEXT( "UPNP supported device found!" ) );
	}
	DeviceList.Empty(); // not for remove port ?
	ForwardPortResult.ExecuteIfBound( bPortForward );
}

bool UUPNP::OpenPort( const DeviceData& DeviceService )
{
	FString XMLFile = RequestXMLFile( DeviceService, RequestType::ACTION );

	if( !XMLFile.IsEmpty() )
	{
		FXmlFile ControlDescription( XMLFile, EConstructMethod::ConstructFromBuffer );
		if( ControlDescription.IsValid() )
		{
			FXmlNode* TargetNode = ControlDescription.GetRootNode()->FindChildNode( TEXT( "s:Body" ) );
			if( TargetNode != nullptr )
			{
				TargetNode = TargetNode->FindChildNode( "u:AddPortMappingResponse" );
				if( TargetNode != nullptr )
				{
					return true;
				}
				else UE_LOG( LogTemp, Log, TEXT( "Node not found ('U:AddPortMappingResponse')" ) );
			}
			else UE_LOG( LogTemp, Log, TEXT( "Node not found ('s:Body')" ) );
		}
		else UE_LOG( LogTemp, Log, TEXT( "FAILED: Parse XML File (recived) from Device(%s)" ), *DeviceService.ControlPath );
	}
	else UE_LOG( LogTemp, Log, TEXT( "FAILED: load xml file" ) );

	return false;
}

bool UUPNP::Discover( const int32 AnswerPort )
{
	if( !Complete_Handle.IsValid() )
	{
		bool bIsValid = false;
		DeviceList.Empty();

		UE_LOG( LogTemp, Log, TEXT( "UPNP Start Discovery" ) );
		// Setup Address
		TSharedPtr<FInternetAddr> TargetAddr = ISocketSubsystem::Get( PLATFORM_SOCKETSUBSYSTEM )->CreateInternetAddr( 0, 1900 );
		TargetAddr->SetIp( TEXT( "239.255.255.250" ), bIsValid );

		// Create Socket -> Setup for UPNP search
		Sock_Ptr = FUdpSocketBuilder( "UPNP_Search" ).AsReusable().WithBroadcast().AsNonBlocking().WithSendBufferSize( 512 ).WithReceiveBufferSize( 102400 ).BoundToEndpoint( FIPv4Endpoint( FIPv4Address::Any, AnswerPort ) );
		if( Sock_Ptr != nullptr && 
			Sock_Ptr->JoinMulticastGroup( *TargetAddr ) && 
			Sock_Ptr->SetMulticastTtl( 2 ) )
		{
			FString SendBuffer( SEARCH_REQUEST_STRING );
			FArrayWriter DataWriter;
			int32 BytesSent = 0;

			// Write to binary -> send data ( ignore unreal stuff )
			DataWriter << SendBuffer;
			if( Sock_Ptr->SendTo( DataWriter.GetData() + 4, DataWriter.Num() - 5, BytesSent, *TargetAddr ) )
			{
				UE_LOG( LogTemp, Log, TEXT( "SUCCESS : %d Bytes was send to UPNP Gateway" ), BytesSent );
				return true;
			}
		}
		else UE_LOG( LogTemp, Log, TEXT( "FAILED : Setup TCP Socket" ) );
		CloseSock();
	}
	else UE_LOG( LogTemp, Log, TEXT( "Cant do this while another search is not completed..." ) );
	return false;
}

int32 UUPNP::FindStrInArray( const FArrayReader& TargetArray, const FString& TargetString, int32 EndIndex, int32 StartIndex /*= 0 */ )
{
	if( StartIndex < 0 || TargetArray.Num() == 0 || TargetString.Len() == 0 || TargetArray.Num() < TargetString.Len() || TargetArray.Num() <= StartIndex || EndIndex < StartIndex )
		return INDEX_NONE;

	for( int32 i = StartIndex; i < EndIndex - TargetString.Len(); ++i )
	{
		if( TargetArray[i] == TargetString[0] )
		{
			bool bFound = true;
			for( int32 j = 1; j < TargetString.Len(); ++j )
			{
				if( TargetArray[i + j] != TargetString[j] )
				{
					bFound = false;
					break;
				}
			}
			if( bFound ) return i;
			i += TargetString.Len() - 1;
		}
	}
	return INDEX_NONE;
}

bool UUPNP::CopyArrayToStr( const FArrayReader& TargetArray, FString& TargetString, int32 EndIndex, int32 StartIndex /*= 0 */ )
{
	if( StartIndex < 0 || EndIndex <= StartIndex || TargetArray.Num() < EndIndex )
		return false;
	TargetString.Empty( EndIndex - StartIndex );
	TargetString.Append( TEXT( " " ), EndIndex - StartIndex );
	for( int32 i = StartIndex; i < EndIndex; ++i )
	{
		TargetString[i - StartIndex] = TargetArray[i];
	}
	return true;
}

FString UUPNP::RequestXMLFile( const DeviceData& Device, RequestType Type /*= GET */ )
{
	TSharedPtr<FInternetAddr> TargetAddr = ISocketSubsystem::Get( PLATFORM_SOCKETSUBSYSTEM )->CreateInternetAddr( Device.Address, Device.Port );
	int32 BytesSent;

	Sock_Ptr = FTcpSocketBuilder( "UPNP Device Analyser" ).AsBlocking().WithSendBufferSize( 10240 ).WithReceiveBufferSize( 102400 ).BoundToEndpoint( FIPv4Endpoint( FIPv4Address::Any, CommunicationPort ) );
	if( Sock_Ptr != nullptr && Sock_Ptr->Connect( *TargetAddr ) )
	{
		FArrayWriter DataWriter;
		FString HTTPRequest;
		FIPv4Address HostAddress( Device.Address );

		switch( Type )
		{
		case GET:
			HTTPRequest = FString::Printf( HTTP_GET_TEMPLATE, *Device.InfoPath, *HostAddress.ToString(), Device.Port );
			break;
		case ACTION:
			bool bCanBind = false;
			TSharedPtr<FInternetAddr> LocalIP = ISocketSubsystem::Get( PLATFORM_SOCKETSUBSYSTEM )->GetLocalHostAddr( *GLog, bCanBind );
			if( !bCanBind ) UE_LOG( LogTemp, Log, TEXT( "Cant get local IP, port mapping will fail" ) );
			FString TypeString;
			switch( Device.Service )
			{
			case DeviceData::WANIP:
				TypeString = SEARCH_SERVICE_TYPE_0;
				break;
			case DeviceData::WANPP:
				TypeString = SEARCH_SERVICE_TYPE_1;
				break;
			default:
				UE_LOG( LogTemp, Log, TEXT( "Invalid ServiceType - cant generate soap request" ) );
				break;
			}
			HTTPRequest = FString::Printf( PARAM_APP_PORT_MAPPING, RequestedPort, TEXT( "TCP" ), RequestedPort, *LocalIP->ToString( false ) );
			HTTPRequest = FString::Printf( XML_APP_PORT_MAPPING, *TypeString, *HTTPRequest );
			HTTPRequest = FString::Printf( HTTP_SOAP_HEADER, *Device.ControlPath, *HostAddress.ToString(), Device.Port, *TypeString, HTTPRequest.Len(), *HTTPRequest );
			break;
		}

		DataWriter.SetNumUninitialized( HTTPRequest.Len() );
		DataWriter << HTTPRequest;

		if( Sock_Ptr->Send( DataWriter.GetData() + 4, DataWriter.Num() - 5, BytesSent ) )
		{
			FArrayReader DataReader;
			DataReader.SetNumUninitialized( 10240 ); // reduce after testing
			UE_LOG( LogTemp, Log, TEXT( "Send %d Bytes to Device(%s)" ), BytesSent, *HostAddress.ToString() );

			if( Sock_Ptr->Recv( DataReader.GetData(), DataReader.Num(), BytesSent ) )
			{
				UE_LOG( LogTemp, Log, TEXT( "%d Bytes recived from Device(%s)" ), BytesSent, *HostAddress.ToString() );
				CloseSock();

				int32 CheckEnd = ( BytesSent > 200 ) ? ( 200 ) : ( BytesSent );
				int32 FindContentLength = FindStrInArray( DataReader, TEXT( "Content-Length: " ), CheckEnd, 0 );
				if( FindContentLength != INDEX_NONE )
				{
					int32 FindContentLengthEnd = FindStrInArray( DataReader, TEXT( "\r" ), CheckEnd, FindContentLength + 17 );
					if( FindContentLengthEnd != INDEX_NONE )
					{
						FString ContentLength;
						if( CopyArrayToStr( DataReader, ContentLength, FindContentLengthEnd, FindContentLength + 16 ) )
						{
							FString XmlFileContent;
							if( CopyArrayToStr( DataReader, XmlFileContent, BytesSent, BytesSent - FCString::Atoi( *ContentLength ) ) )
							{
								return XmlFileContent;
							}
							else UE_LOG( LogTemp, Log, TEXT( "Cant create XML-File Buffer" ) );
						}
						else UE_LOG( LogTemp, Log, TEXT( "FAILED: read contentlength" ) );
					}
					else UE_LOG( LogTemp, Log, TEXT( "FAILED: find contentlength end" ) );
				}
				else UE_LOG( LogTemp, Log, TEXT( "FAILED: find Content-Lenth in HTTP Request from Device(%s)" ), *HostAddress.ToString() );
			}
			else UE_LOG( LogTemp, Log, TEXT( "FAILED: recive data from Device(%s)" ), *HostAddress.ToString() );
		}
		else UE_LOG( LogTemp, Log, TEXT( "FAILED: sent to Device : %s" ), *HostAddress.ToString() );
	}
	else UE_LOG( LogTemp, Log, TEXT( "FAILED: connect to device %d:%d" ), Device.Address, Device.Port );
	
	CloseSock();
	return FString();
}

UUPNP::UUPNP()
{
	Complete_Handle.Invalidate();
	Checker_Handler.Invalidate();
}

UUPNP::~UUPNP()
{
	CloseSock();
}
