// Fill out your copyright notice in the Description page of Project Settings.

#include "CardGame.h"
#include "CGGameInstance.h"

UUPNP* UCGGameInstance::GetUPNP()
{
	return UPNP;
}

void UCGGameInstance::InitUPNP()
{
	UPNP = NewObject<UUPNP>( this );
	UPNP->ForwardPortResult.BindUObject( this, &UCGGameInstance::ForwardPortResult );
}
