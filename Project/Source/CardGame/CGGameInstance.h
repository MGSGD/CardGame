#pragma once

#include "Engine/GameEngine.h"

#include "UPNP.h"

#include "CGGameInstance.generated.h"

UCLASS()
class CARDGAME_API UCGGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
	UUPNP*	UPNP;

public:
	UFUNCTION( BlueprintPure, Category = "UPNP" )
	UUPNP* GetUPNP();

	UFUNCTION( BlueprintCallable, Category = "UPNP" )
	void InitUPNP();

	UFUNCTION( BlueprintImplementableEvent, Category = "UPNP" )
	void ForwardPortResult( bool Result );
};
