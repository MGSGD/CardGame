#pragma once

#include "CGInformations.h"
#include "CGMonster.generated.h"

class ACGGameState;

USTRUCT(BlueprintType)
struct FMonster
{
	GENERATED_BODY()

	ACGGameState* GameState;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Monster" )
	int32 Index = 0;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Loot Informations")
	FCard LootInformations;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Card Informations" )
	FCard CardInformations;

	void CreateMonsterCard(int32 Level = 1);
	int32 RollAttack();
	FLoot SlayMonster();
};
