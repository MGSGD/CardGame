#pragma once

enum class EAbilityFlags
{
	None = 0x00,
	CanAttack = 0x01,
	UseSlotAbility = 0x02,
	NotActivateable = 0x04,
	OnlyMonsterTarget = 0x08,
	OnlyPlayerTarget = 0x10,
	// E = 0x20,
	// E = 0x40,
	// E = 0x80,
};

ENUM_CLASS_FLAGS( EAbilityFlags )

enum class ESlotAbilities : uint8 // 0 - 99
{
	INVALID = 0,
	
	// Weapon - Abilities
	INCREASE_EACH_ATTACK_BY_ONE = 1, // increase attack +1 for each card attack card
	REDUCE_ACTION_AP_COST = 13, // redruce ap cost of every action card played ( min 1 ap cost )
	HIT_ADDITIONAL_MONSTER = 15, // attack a additional monster with {Param}% damage
	HIT_TWO_ADD_MONSTER = 16, // kill attacked monster
	REDUCE_AP_INCREASE_DMG = 17, // same as REDRUCE_ACTION_AP_COST with increase attack
	DISCARD_ACTION_REFILL_SLOT_AP = 18, // discard actioncards for refill slot ap on self attack
	INCREASE_AP_FOR_MONSTER = 3,
	PLAY_ATTACK_CARD_TWICE = 2, // next action card played will be added twice

	// Equipment - Abilities
	GET_PART_OF_OPPONENT_GOLD_LOOT = 4, // get {Param} of gold looted by your opponent
	GET_ONE_CARD_MORE_BUY = 5, // increase card selection size by one
	SHOP_COST_REDUCED = 6, // reduce cost of one shop buy each round
	GET_GOLD_EACH_ATTACK_CARD = 7, // get gold for each attack card played in your round
	ALL_MONSTER_GIVE_GOLD = 8, // get gold for each monster killed ( include opponen monster )
	MULTIPLY_GOLD_DROP = 9, // multiply gold drop by monster
	GOLD_AT_TURN_END = 10, // get gold at end of every turn
	GOLD_FOR_AP_TURN_END = 11, // get gold for each AP at the end of your turn
	GOLD_FOR_ACTION_OPPONENT = 12, // get gold for each action card is played against a opponent
};

enum class EActionAbilities : uint8 // 100 - 999
{
	INVALID = 100,

	// Weapon - Abilities
	DRAW_TWO_ATTACK_CARDS = 101, // draw two attack card and play them for no cost
	PLAY_SPELL_FREE = 113, // play next spell free
	DISCARD_FOR_DMG = 114, // get 1 dmg on slot for each card discareded
	DMG_FOREACH_HAND_CARD = 115, // get {Param} dmg on slot foreach card in hand
	DESTROY_OPPONENT_ACTION_CARD = 116, // destroy a actioncard from your opponent hand
	INCREASE_WEAPON_AP = 117, // increase weapon ap by one untill turnend

	// Equipment - Abilities
	DISCARD_FOR_FREE_PLAY_ONCE = 102, // Same as DISCARD_FOR_TWIC_PLAY but only once
	DUPLICATE_OPPONENT_SLOT_ITEM = 103, // Get SlotParam Item ( opponent ) and equip it
	PLAY_ACTION_CARD_TWICE = 104, // Play Action card twice
	INCREASE_AP_NEXT_TURN = 105, // increase the ap by one for the selected slot in next turn
	GOLD_EACH_CARD_ON_ATTACK = 106, // get gold for each attack card on a attack
	DISCARD_FOR_NEW_ONE = 107, // get for each card discarded a new card of same type
	SET_SLOT_AP_FOR_NEXT_TURN = 108, // set the ap of a opponent slot to {Param}
	PLAY_CARD_FREE = 109, // play next actioncard for free
	INCREASE_AP_UNTIL_TURN_END = 110, // increase ap of slot for current turn
	INCREASE_AP_FOREACH_CARD = 111, // incrase ap of active slot foreach card in this slot
	REDUCE_ACTION_AP_COST = 112, // Same as REDUCE_ACTION_AP_COST - use him ID maybe ?
};

#define ASINT( EnumEntry ) static_cast<int32>( EnumEntry )
#define ASSLOT( EnumEntry ) static_cast<ESlotAbilities>( EnumEntry )
#define ASACTION( EnumEntry ) static_cast<EActionAbilities>( EnumEntry )

// TODO : Implement 105, 108
