#pragma once

#include "Engine/DataTable.h"
#include "CGInformations.generated.h"

UENUM(BlueprintType)
namespace ECGCard {
	enum Type {
		CGNone = 0			UMETA(DisplayName = "None"),
		CGWeapon = 1 		UMETA(DisplayName = "Weapon"),
		CGEquipment = 2		UMETA(DisplayName = "Equipment"),
		CGGold = 3			UMETA(DisplayName = "Gold"),
		CGMonster = 4		UMETA(DisplayName = "Monster")
	};
}

UENUM(BlueprintType)
namespace ECGRarity
{
	enum Type {
		CGBronze = 0		UMETA( DisplayName = "Bronze" ),
		CGSilver = 1		UMETA( DisplayName = "Silver" ),
		CGGold = 2			UMETA( DisplayName = "Gold" )
	};
}

UENUM(BlueprintType)
namespace ECGSlot {
	enum Type {
		CGWeaponSlot	= 0 	UMETA(DisplayName = "Weapon Slot"),
		CGShieldSlot	= 1		UMETA(DisplayName = "Shield Slot"),
		CGEquipmentSlot = 2		UMETA(DisplayName = "Equipment Slot"),
	};
}

USTRUCT(BlueprintType)
struct FCard{
	GENERATED_BODY()
public:
	FCard()
		: CardType(ECGCard::CGNone)
		, Name("Default")
		, Discription("")
		, Level(1)
		, AbilityID (-1)
		, AP(0)
		, Value(0)
		, bIsSlot(false)
		, Param0(0)
		, Rarity( ECGRarity::CGBronze )
		, Texture( nullptr )
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Card Info")
		TEnumAsByte<ECGCard::Type> CardType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Card Info")
		FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Card Info")
		FString Discription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Card Info")
		int32 Level;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Card Info" )
		int32 AbilityID;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Texture" )
		class UTexture2D*	Texture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Card Info")
		int32 AP;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Card Info" )
		int32 Value;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Card Info" )
		TEnumAsByte<ECGRarity::Type> Rarity;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Card Info" )
		bool bIsSlot;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Card Info" )
		int32 Param0;

	void CopyFromOtherCard(const FCard& Card) {
		CardType = Card.CardType;
		Name = Card.Name;
		Discription = Card.Discription;
		Level = Card.Level;
		AbilityID = Card.AbilityID;
		AP = Card.AP;
		Value = Card.Value;
		bIsSlot = Card.bIsSlot;
		Param0 = Card.Param0;
		Rarity = Card.Rarity;
		Texture = Card.Texture;
	}
	
	bool IsValidCard() {
		return CardType != ECGCard::CGNone;
	}

	bool CanBeActivated()
	{
		return false;
	}

	void EmptyCard() {
		CardType = ECGCard::CGNone;
		Name = "Default";
		Discription = "Lorem Ipsum";
		Level = 1;
		AbilityID = -1;
		AP = 0;
		Value = 0;
		bIsSlot = false;
		Rarity = ECGRarity::CGBronze;
	}

	FORCEINLINE bool operator==( const FCard& RightCard) const
	{
		return( CardType == RightCard.CardType && Name == RightCard.Name &&
			Discription == RightCard.Discription && Level == RightCard.Level && 
			AbilityID == RightCard.AbilityID && AP == RightCard.AP &&
			Rarity == RightCard.Rarity && Value == RightCard.Value &&
			bIsSlot == RightCard.bIsSlot && Param0 == RightCard.Param0 );
	}
};

UENUM(BlueprintType)
namespace ECGPhase {
	enum Type {
		CGEquipPhase	= 0 	UMETA(DisplayName = "Equip Phase"),
		CGAttackPhase	= 1		UMETA(DisplayName = "Attack Phase"),
		CGShopPhase		= 2		UMETA(DisplayName = "Shop Phase"),
	};
}

USTRUCT(BlueprintType)
struct FMonsterData : public FTableRowBase {
	GENERATED_BODY()

public:

	FMonsterData()
		: Level(-1)
		, DamageLow(0)
		, DamageMid(0)
		, DamageHigh(0)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
		int32 Level;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roll")
		int32 DamageLow;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roll")
		int32 DamageMid;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roll")
		int32 DamageHigh;
};

USTRUCT(BlueprintType)
struct FLootData : public FTableRowBase {
	GENERATED_BODY()

public:

	FLootData()
		: Level(-1)
		, Chance_Silver(0)
		, Chance_Gold(0)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
		int32 Level;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roll")
		int32 Chance_Silver;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roll")
		int32 Chance_Gold;
};

USTRUCT(BlueprintType)
struct FGoldData : public FTableRowBase {
	GENERATED_BODY()

public:

	FGoldData()
		: Level(-1)
		, Gold(-1)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
		int32 Level;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gold")
		int32 Gold;
};

struct FLoot{
	FLoot()
		: Card(FCard())
		, Gold(-1)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Card")
		FCard Card;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gold")
		int32 Gold;
};

USTRUCT(BlueprintType)
struct FCardData : public FTableRowBase {
	GENERATED_BODY()

public:
	FCardData()
	: CardID( 0 )
	, Name( "Default" )
	, DescriptionBronze( "Description" )
	, DescriptionSilver( "Description" )
	, DescriptionGold( "SDescription" )
	, Value( 1 )
	, Level( 0 )
	, EffectIDBronze( -1 )
	, EffectIDSilver( -1 )
	, EffectIDGold( -1 )
	, ParamBronze0( 0 )
	, ParamSilver0( 0 )
	, ParamGold0( 0 )
	, APBronze(0)
	, APSilver(0)
	, APGold(0)
	, Texture( nullptr )
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ID")
		int32 CardID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Name")
		FString Name;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Level" )
		int32 Level;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Effect" )
		int32 EffectIDBronze;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Effect" )
		int32 EffectIDSilver;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Effect" )
		int32 EffectIDGold;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Actionpoints" )
		int32 APBronze;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Actionpoints" )
		int32 APSilver;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Actionpoints" )
		int32 APGold;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Description" )
		FString DescriptionBronze;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Description" )
		FString DescriptionSilver;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Description" )
		FString DescriptionGold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Charges")
		int32 Value;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Effect" )
		int32 ParamBronze0;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Effect" )
		int32 ParamSilver0;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Effect" )
		int32 ParamGold0;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Texture" )
		class UTexture2D* Texture;
		//FString Texture;

	FString GetDescription( ECGRarity::Type Rarity ) const
	{
		int32 pos = 0;
		switch( Rarity )
		{
			case ECGRarity::CGBronze :
				pos = DescriptionBronze.Find( "%d" );
				if( pos != INDEX_NONE )
				{
					if( DescriptionBronze.Find( "%d", ESearchCase::CaseSensitive, ESearchDir::FromStart, pos + 2 ) != INDEX_NONE )
					{
						return FString::Printf( *DescriptionBronze, ParamBronze0, ParamBronze0 );
					}
					return FString::Printf( *DescriptionBronze, ParamBronze0 );
				}
				return DescriptionBronze;
			case ECGRarity::CGSilver :
				pos = DescriptionSilver.Find( "%d" );
				if( pos != INDEX_NONE )
				{
					if( DescriptionSilver.Find( "%d", ESearchCase::CaseSensitive, ESearchDir::FromStart, pos + 2 ) != INDEX_NONE )
					{
						return FString::Printf( *DescriptionSilver, ParamSilver0, ParamSilver0 );
					}
					return FString::Printf( *DescriptionSilver, ParamSilver0 );
				}
				return DescriptionSilver;
			case ECGRarity::CGGold :
				pos = DescriptionGold.Find( "%d" );
				if( pos != INDEX_NONE )
				{
					if( DescriptionGold.Find( "%d", ESearchCase::CaseSensitive, ESearchDir::FromStart, pos + 2 ) != INDEX_NONE )
					{
						return FString::Printf( *DescriptionGold, ParamGold0, ParamGold0 );
					}
					return FString::Printf( *DescriptionGold, ParamGold0 );
				}
				return DescriptionGold;
		}
		return DescriptionBronze;
	}
	FORCEINLINE const int32& GetEffectID( ECGRarity::Type Rarity ) const
	{
		switch( Rarity )
		{
			case ECGRarity::CGBronze:
				return EffectIDBronze;
			case ECGRarity::CGSilver:
				return EffectIDSilver;
			case ECGRarity::CGGold:
				return EffectIDGold;
		}
		return EffectIDBronze;
	}
	FORCEINLINE const int32& GetParam( ECGRarity::Type Rarity ) const
	{
		switch( Rarity )
		{
			case ECGRarity::CGBronze:
				return ParamBronze0;
			case ECGRarity::CGSilver:
				return ParamSilver0;
			case ECGRarity::CGGold:
				return ParamGold0;
		}
		return ParamBronze0;
	}
	FORCEINLINE const int32& GetAP( ECGRarity::Type Rarity ) const
	{
		switch( Rarity )
		{
		case ECGRarity::CGBronze:
			return APBronze;
		case ECGRarity::CGSilver:
			return APSilver;
		case ECGRarity::CGGold:
			return APGold;
		}
		return APBronze;
	}
};

// Ability Mask
UENUM(BlueprintType)
namespace ECGAction
{
	enum Type
	{
		CGNone		= 0,
		CGPlay		= 1	UMETA( DisplayName = "Play Card" ),
		CGEquip		= 2	UMETA( DisplayName = "Equip Card" ),
		CGSelect	= 3	UMETA( DisplayName = "Select Card" ),
		CGAttack	= 4 UMETA( DisplayName = "Attack Target" ),
		CGDiscard	= 5 UMETA( DisplayName = "Discard Card" )
	};
}

UENUM( BlueprintType )
enum EAbbilityTargets
{
	None				= 0x00 UMETA( DisplayName = "Clear Flag" ),
	Weapon				= 0x01 UMETA( DisplayName = "Weapon Slot" ),
	WeaponOpponent		= 0x02 UMETA( DisplayName = "Weapon Slot Opponent" ),
	Equipment			= 0x04 UMETA( DisplayName = "Equipment Slot" ),
	EquipmentOpponent	= 0x08 UMETA( DisplayName = "Equipment Slot Opponent" ),
	EquipmentActions	= 0x10 UMETA( DisplayName = "Equipment Actions" ),
	WeaponActions		= 0x20 UMETA( DisplayName = "Weapon Actions" ),
	Monser				= 0x40 UMETA( DisplayName = "Monster Target" ),
	Player				= 0x80 UMETA( DisplayName = "Player Target" )
};

#define HAS_FLAG( Variable, Flag ) ( ( Variable & Flag ) == Flag )
#define SET_FLAG( Variable, Flag ) ( Variable |= Flag )
#define CLR_FLAG( Variable, Flag ) ( Variable = Variable & ~Flag )

ENUM_CLASS_FLAGS( EAbbilityTargets )

USTRUCT(BlueprintType)
struct FAbilityMask : public FTableRowBase
{
	GENERATED_BODY()

public:
	FAbilityMask() 
		: ID( 0 )
		, AbilityID( 0 )
		, Counter( 1 )
		, Action( ECGAction::CGNone )
		, TargetFlag( EAbbilityTargets::None )
		, ChainID( -1 )
		{}

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "ID" )
		int32							ID;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "AbilityID" )
		int32							AbilityID;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Counter" )
		int32							Counter;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Action" )
		TEnumAsByte<ECGAction::Type>	Action;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Target Mask" )
		int32							TargetFlag;
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Chain" )
		int32							ChainID;
};

