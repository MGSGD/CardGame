// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CGInformations.h"
#include "CGCard.generated.h"

UCLASS()
class CARDGAME_API ACGCard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACGCard();

	UFUNCTION( BlueprintImplementableEvent, Category = "Card Information" )
	void UpdateUI( const FCard& Information );

	UFUNCTION( BlueprintCallable, Category = "Card Information" )
	void UpdateCard( const FCard& Information );

	UFUNCTION( BlueprintPure, Category = "Card Information" )
	bool IsValid( const FCard& Information ) const;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Card Informations" )
	int32	Index;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Card Informations" )
	bool	Owning;
};
