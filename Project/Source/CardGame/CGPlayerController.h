#pragma once

#include "GameFramework/PlayerController.h"

#include "CGPlayerController.generated.h"

class ACGMonster;
class ACGGameState;
class ACGPlayerController;
class ACGPlayerState;
struct FCard;

/**
 * 
 */
UCLASS()
class CARDGAME_API ACGPlayerController : public APlayerController
{
	GENERATED_BODY()

	ACGGameState* GameState;
	ACGPlayerState* CPlayerState;
	ACGPlayerState* OPlayerState;

	FMonster* GetMonster( int32 Index );

public:
	ACGPlayerController(const FObjectInitializer& ObjectInitializer);

	virtual void SetupInputComponent() override;

	UFUNCTION(reliable, server, WithValidation)
		void ServerStartGame();
	UFUNCTION(BlueprintCallable, Category = "Game State")
		void StartGame();

	UFUNCTION(reliable, server, WithValidation)
		void ServerEndPhase();
	UFUNCTION(BlueprintCallable, Category = "Game State")
		bool EndPhase();

	UFUNCTION(reliable, server, WithValidation)
		void ServerEndTurn();
	UFUNCTION(BlueprintCallable, Category = "Game State")
		bool EndTurn();

	UFUNCTION(reliable, server, WithValidation)
		void ServerDiscardCard(int32 HandIndex);
	UFUNCTION(BlueprintCallable, Category = "Player Action")
		bool DiscardCard(int32 HandIndex);

	UFUNCTION( Reliable, Server, WithValidation )
		void ServerSelectSlot( ECGCard::Type SlotType, bool Owning );
	UFUNCTION( Reliable, Client )
		void OwnerSelectSlotResult( bool Result );
	UFUNCTION( BlueprintCallable, Category = "Player Action" )
		void SelectCard( ECGCard::Type SlotType, bool Owning );
	UFUNCTION( BlueprintImplementableEvent, Category = "Player Action" )
		void SelectCardResult( bool Result );

	UFUNCTION(reliable, server, WithValidation)
		void ServerAttackMonster( int32 monster);
	UFUNCTION( reliable, client )
		void OwnerAttackMonsterResult( int32 Result );
	UFUNCTION(BlueprintCallable, Category = "Player Action")
		void AttackMonster( int32 monster); // ... - -2 ^= Gold, -1 ^= failed, 0 - ... ^= Damage got 
	UFUNCTION( BlueprintImplementableEvent, Category = "Player Action" )
		void AttackMonsterResult( int32 Value ); 

	UFUNCTION(reliable, server, WithValidation)
		void ServerAttackPlayer( ACGPlayerState* ps );
	UFUNCTION( reliable, client )
		void OwnerAttackPlayerResult( int32 Result );
	UFUNCTION( BlueprintCallable, Category = "Player Action" )
		void AttackPlayer( ACGPlayerState* ps );
	UFUNCTION( BlueprintImplementableEvent, Category = "Player Action" )
		void AttackPlayerResult( int32 Value );

	UFUNCTION(reliable, server, WithValidation)
		void ServerEquipCard(FCard card);
	UFUNCTION( reliable, client )
		void OwnerEquipCardResult( bool Result );
	UFUNCTION(BlueprintCallable, Category = "Player Action")
		void EquipCard(FCard card);
	UFUNCTION( BlueprintImplementableEvent, Category = "Player Action" )
		void EquipCardResult( bool Result );

	UFUNCTION(reliable, server, WithValidation)
		void ServerClearShopSelection();
	UFUNCTION(BlueprintCallable, Category = "Player Action")
		bool ClearShopSelection();

	UFUNCTION(reliable, server, WithValidation)
		void ServerAddCardFromSelection(int32 Index);
	UFUNCTION(BlueprintCallable, Category = "Player Action")
		bool AddCardFromSelection(int32 Index);

	UFUNCTION(reliable, server, WithValidation)
		void ServerBuyCard( ECGCard::Type Type );
	UFUNCTION( Reliable, Client )
		void OwnerBuyCardResult( int32 Result );
	UFUNCTION(BlueprintCallable, Category = "Player Action")
		void BuyCard( ECGCard::Type Type );
	UFUNCTION( BlueprintImplementableEvent, BlueprintCallable, Category = "Player Action" )
		void BuyCardResult( int32 Result );
	UFUNCTION( Reliable, Client, Category = "Player Action" )
		void OwnerShopSelectionResult( const TArray<FCard>& CardSelection );
	UFUNCTION( BlueprintImplementableEvent, Category = "Player Action" )
		void ShopCardSelection( const TArray<FCard>& CardSelection );

	UFUNCTION(reliable, server, WithValidation)
		void ServerCastSpell(FCard card, int32 monster);
	UFUNCTION( reliable, client )
		void OwnerCastSpellResult( int32 Result );
	UFUNCTION(BlueprintCallable, Category = "Player Action")
		void CastSpell(FCard card, int32 monster);
	UFUNCTION( BlueprintImplementableEvent, Category = "Player Action" )
		void CastSpellResult( int32 Result );

	UFUNCTION( reliable, server, WithValidation )
		void ServerActivateSlot( ECGCard::Type Slot );
	UFUNCTION( BlueprintCallable, Category = "Player Action" )
		void ActivateSlot( ECGCard::Type Slot );

	UFUNCTION(BlueprintPure, Category = "Player State")
		bool GetIsActivePlayer();

	UFUNCTION(exec)
		void cvar_EndPhase();

	UFUNCTION( BlueprintCallable, Category = "Log" )
		void AddLogEntry( bool Owning, ECGAction::Type Action, FCard Source, FCard Target, int32 Result, FCard Loot );

	UFUNCTION( Server, Reliable, WithValidation )
		void Server_AddLog( bool Owning, ECGAction::Type Action, FCard Source, FCard Target, int32 Result, FCard Loot );

	UFUNCTION( Client, Reliable )
		void Client_AddLog( bool Owning, ECGAction::Type Action, FCard Source, FCard Target, int32 Result, FCard Loot );

	// Helper Commands
	UFUNCTION( exec ) void AddSlotWeapon( int32 ID );
	UFUNCTION( exec ) void AddWeapon( int32 ID );
	UFUNCTION( exec ) void AddSlotEquipment( int32 ID );
	UFUNCTION( exec ) void AddEquipment( int32 ID );
	UFUNCTION( exec ) void AddGold( int32 Amount );

	UFUNCTION( Server, Reliable, WithValidation )
		void ServerAddGold( int32 Amount );

	UFUNCTION( Reliable, Server, WithValidation )
	void CheckAndAdd( ECGCard::Type Slot, bool Equip, int32 ID );

	void AddSpecificCard( const FCardData& Data, ECGCard::Type Type, ECGRarity::Type Rarity, bool bSlot );

	void NetInit();
};
