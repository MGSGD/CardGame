#pragma once

#include "Object.h"

#include "Networking.h"
#include "XmlParser.h"

#include "UPNP.generated.h"

struct DeviceData
{
	enum ServiceType
	{
		NONE = 0x01,
		WANIP = 0x02,
		WANPP = 0x03
	};

	uint32	Address = 0;
	int32	Port = 0;
	ServiceType Service = NONE;

	FString InfoPath;
	FString ControlPath;

	FORCEINLINE bool operator==( const DeviceData& Other ) const
	{
		return( Other.Address == Address && Other.Port == Port );
	}
};

DECLARE_DELEGATE_OneParam( ForwardPortDelegate, bool )

UCLASS()
class CARDGAME_API UUPNP : public UObject
{
	GENERATED_BODY()

	enum RequestType
	{
		GET = 0,
		ACTION = 1
	};

	float	Duration = 4.f;
	float	Intervall = 0.3f;
	int32	CommunicationPort = 6000;
	int32	RequestedPort = 7777;

	int32	CurrentDevice = 0;

	FSocket*		Sock_Ptr = nullptr;
	FTimerHandle	Complete_Handle;
	FTimerHandle	Checker_Handler;
	
	TArray<DeviceData> DeviceList;


	void CheckDeviceAdvertise();
	void CompleteDiscovery();

	void CheckDeviceService();
	void CompleteAnalyse();

	bool OpenPort( const DeviceData& DeviceService );
	bool Discover( const int32 AnswerPort = 6000 );

	int32 FindStrInArray( const FArrayReader& TargetArray, const FString& TargetString, int32 EndIndex, int32 StartIndex = 0 );
	bool CopyArrayToStr( const FArrayReader& TargetArray, FString& TargetString, int32 EndIndex, int32 StartIndex = 0 ); // Last char is ignored - EndIndex + 1 if you need it!
	FString RequestXMLFile( const DeviceData& Device, RequestType Type = GET );
	void CloseSock();
	FORCEINLINE int32 FindCharInStr( const FString& Target, const char SearchTarget, const uint32 StartIndex = 0 ) const
	{
		for( int32 i = StartIndex; i < Target.Len(); ++i )
		{
			if( Target[i] == SearchTarget ) return i;
		}
		return INDEX_NONE;
	}

	class UWorld* GetWorld() const
	{
		return this->GetOuter()->GetWorld();
	}

public:
	UFUNCTION( BlueprintCallable, Category = "UPNP" )
	bool ForwardPort( const int32 Port = 7777, const float CheckIntervall = 0.3f, const float FullDuration = 4.f );

	ForwardPortDelegate ForwardPortResult;

	UUPNP();
	~UUPNP();
};
