#include "CardGame.h"
#include "CGMonster.h"

void FMonster::CreateMonsterCard(int32 Level) {
	CardInformations.CardType = ECGCard::CGMonster;
	CardInformations.Name = "Random Monster";
	CardInformations.Discription = "Random Discription";
	CardInformations.Level = Level;
	
	FCard OldLoot = LootInformations;
	LootInformations = FCard();
	LootInformations.CardType = static_cast<ECGCard::Type>( FMath::RandRange( 1, 3 ) ); // Roll Card Type
	LootInformations.Level = Level;

	TArray<FCardData>* LootData = nullptr;
	uint32* LevelCount = nullptr;
	switch( LootInformations.CardType )
	{
	case ECGCard::CGWeapon:
		LootData = &GameState->WeaponSlotData;
		LevelCount = reinterpret_cast<uint32*>( GameState->WeaponSlotLevelCount );
		break;
	case ECGCard::CGEquipment:
		LootData = &GameState->EquipmentSlotData;
		LevelCount = reinterpret_cast<uint32*>( GameState->EquipmentSlotLevelCount );
		break;
	}

	// roll rarity
	int32 Roll = FMath::RandRange( 0, 100 );
	if( Roll > 100 - GameState->LootData[CardInformations.Level].Chance_Gold ) LootInformations.Rarity = ECGRarity::CGGold;
	else if( Roll > 100 - GameState->LootData[CardInformations.Level].Chance_Gold - GameState->LootData[CardInformations.Level].Chance_Silver ) LootInformations.Rarity = ECGRarity::CGSilver;
	
	if( LootData != nullptr )
	{
		const TArray<FCardData>& TLoot = *LootData;
		int RandomIndex = GameState->GetRandomIndex( TLoot, CardInformations.Level, LevelCount );
		LootInformations.Name = TLoot[RandomIndex].Name;
		LootInformations.Discription = TLoot[RandomIndex].GetDescription( LootInformations.Rarity );
		LootInformations.Texture = TLoot[RandomIndex].Texture;
		LootInformations.AbilityID = TLoot[RandomIndex].GetEffectID( LootInformations.Rarity );
		LootInformations.Value = TLoot[RandomIndex].Value;
		LootInformations.AP = TLoot[RandomIndex].GetAP( LootInformations.Rarity );
		LootInformations.Param0 = TLoot[RandomIndex].GetParam( LootInformations.Rarity );
		LootInformations.bIsSlot = true;
	}
}

FLoot FMonster::SlayMonster() {
	FLoot Loot;
	Loot.Card = LootInformations;

	Loot.Gold = GameState->GoldData[CardInformations.Level].Gold;
	if( LootInformations.CardType == ECGCard::CGGold )
	{
		Loot.Gold += Loot.Gold * ( static_cast<int32>( LootInformations.Rarity ) + 1 );
	}
	
	CreateMonsterCard(CardInformations.Level + 1);
	GameState->CheckMonsterLevel();
	return Loot;
}

int32 FMonster::RollAttack()
{
	int32 Roll = FMath::RandRange(1, 6);
	if( Roll == 1 || Roll == 2 ) return GameState->MonsterData[CardInformations.Level].DamageLow;
	else if( Roll == 3 || Roll == 4 ) return GameState->MonsterData[CardInformations.Level].DamageMid;
	return GameState->MonsterData[CardInformations.Level].DamageHigh;
}
