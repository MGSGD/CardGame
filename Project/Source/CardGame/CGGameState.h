#pragma once

#include "GameFramework/GameState.h"
#include "CGMonster.h"
#include "CGGameState.generated.h"

class ACGPlayerState;

UCLASS()
class CARDGAME_API ACGGameState : public AGameState
{
	GENERATED_BODY()

public:	
	ACGGameState(const FObjectInitializer& ObjectInitializer);
	
	// Level Depended Data
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Data")
		UDataTable* MonsterDataTable;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Data")
		UDataTable* LootDataTable;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Data")
		UDataTable* GoldDataTable;

	UPROPERTY( Replicated, BlueprintReadOnly, Category = "Data")
		TArray<FLootData> LootData;
	UPROPERTY( Replicated, BlueprintReadOnly, Category = "Data")
		TArray<FMonsterData> MonsterData;
	UPROPERTY( Replicated, BlueprintReadOnly, Category = "Data")
		TArray<FGoldData> GoldData;
	UPROPERTY( Replicated, BlueprintReadOnly, Category = "Data" )
		TArray<FAbilityMask> AbilityMaskData;

	// Cards Data
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Data")
		UDataTable* WeaponDataTable;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Data")
		UDataTable* EquipmentDataTable;
	UPROPERTY( BlueprintReadWrite, EditAnywhere, Category = "Data" )
		UDataTable* EquipmentSlotDataTable;
	UPROPERTY( BlueprintReadWrite, EditAnywhere, Category = "Data" )
		UDataTable* WeaponSlotDataTable;
	UPROPERTY( BlueprintReadWrite, EditAnywhere, Category = "Data" )
		UDataTable* AbilityMasksTable;

	UPROPERTY( BlueprintReadOnly, Category = "Data" )
		TArray<FCardData> WeaponData;
	UPROPERTY( BlueprintReadOnly, Category = "Data" )
		TArray<FCardData> EquipmentData;
	UPROPERTY( BlueprintReadOnly, Category = "Data" )
		TArray<FCardData> WeaponSlotData;
	UPROPERTY( BlueprintReadOnly, Category = "Data" )
		TArray<FCardData> EquipmentSlotData;

	uint32 WeaponLevelCount[5];
	uint32 WeaponSlotLevelCount[5];
	uint32 EquipmentLevelCount[5];
	uint32 EquipmentSlotLevelCount[5];

	UPROPERTY( EditDefaultsOnly, BlueprintReadWrite, Category = "Turn Time" )
		bool bUseTurnTimer;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Turn Time")
		float TurnTimeLimit;
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, ReplicatedUsing = OnRep_CurrentTimeLimit, Category = "Turn Time")
		float CurrentTimeLimit;

	UFUNCTION( BlueprintImplementableEvent )
		void OnRep_CurrentTimeLimit();
		
	void CheckTurnTime(float DeltaTime);

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Game Phase" )
		TEnumAsByte<ECGPhase::Type> CurrentPhase;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Game Phase" )
		bool PlayersTurn;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Game Phase", ReplicatedUsing = OnRep_DungeonLevel)
		int32 DungeonLevel;

	UPROPERTY( ReplicatedUsing=OnRep_GameIsRunning, VisibleAnywhere, BlueprintReadOnly, Category = "Game Phase" )
		bool GameIsRunning;
	
	UPROPERTY(ReplicatedUsing=OnRep_Monster, EditAnywhere, BlueprintReadWrite, Category = "Player Stats") // Remove replication -> local assignment in BP
		TArray<FMonster> Monsters;

	void LoadData();

	void StartGame(ACGPlayerState* PlayerState);
	void EndPhase();

	UFUNCTION()
		void OnRep_DungeonLevel();

	UFUNCTION(BlueprintImplementableEvent, Category = "Game Phase")
		void DungeonLevelChanged(int32 Level);

	UFUNCTION( NetMulticast, Reliable )
		void ServerPhaseEnded( ECGPhase::Type Phase );

	UFUNCTION( NetMulticast, Reliable )
		void ServerPhaseStarted( ECGPhase::Type Phase );

	UFUNCTION(BlueprintImplementableEvent, Category = "Game Phase")
		void PhaseEnded(ECGPhase::Type Phase);

	UFUNCTION(BlueprintImplementableEvent, Category = "Game Phase")
		void PhaseStarted(ECGPhase::Type Phase);

	UFUNCTION( BlueprintImplementableEvent, Category = "Game Phase" )
		void GameHasStarted();

	UFUNCTION( BlueprintImplementableEvent, Category = "Log" )
		void BattleLog_Updated( const FString& Msg );

	UFUNCTION( NetMulticast, Reliable )
		void ServerInitController();

	UFUNCTION( BlueprintImplementableEvent, Category = "Monster" )
		void MonsterUpdate( int32 Index );
	UFUNCTION()
		void OnRep_Monster();

	void AddLog( bool Owning, ECGAction::Type Action, const FCard& Source, const FCard& Target, int32 Result, const FCard& Loot );

	void DrawCard(ACGPlayerState* PlayerState, ECGCard::Type Type);
	void CheckMonsterLevel();

	virtual void Tick(float DeltaTime) override;

	FCard GenerateCard(ECGCard::Type Type, int32 Level, bool Slot = false);

	int32 GetRandomIndex( const TArray<FCardData>& DBType, int32 Level, const uint32* LevelCount ) const;
	const FAbilityMask GetAbilityMask( int32 AbilityID );
	const FAbilityMask GetMaskEntry( int32 ID );
private:
	UFUNCTION()
	void OnRep_GameIsRunning();

	void GenerateStartMonster();
	void GenerateStartHand();
	virtual void GetLifetimeReplicatedProps( TArray<FLifetimeProperty>& OutLifetimeProps ) const;
	
	const FString TypeToString( ECGCard::Type Enum ) const
	{
		switch( Enum )
		{
		case ECGCard::CGNone:
			return "None";
		case ECGCard::CGWeapon:
			return "Weapon";
		case ECGCard::CGEquipment:
			return "Equip";
		case ECGCard::CGGold:
			return "Gold";
		case ECGCard::CGMonster:
			return "Monster";
		default:
			return "Invalid";
		}
	};
};
