#pragma once

#include "GameFramework/PlayerState.h"
#include "CGPlayerState.generated.h"

struct FCard;
struct FMonster;
class ACGMonster;
class ACGPlayerController;
class ACGGameState;

UCLASS()
class CARDGAME_API ACGPlayerState : public APlayerState
{
	GENERATED_BODY()

	ACGGameState*	GameState;
	FCard			LastActionCard;
	
	FCard			WeaponDefaults;
	TArray<FCard>	WeaponAbilities;
	EAbilityFlags	WeaponFlag = EAbilityFlags::None;
	FCard			EquipmentDefaults;
	TArray<FCard>	EquipmentAbilities;
	EAbilityFlags	EquipmentFlag = EAbilityFlags::None;

	int32			AbbilityCounter00 = 0;
	int32			AbbilityCounter01 = 0;


public:
	bool			ExistingCard = true;

	ACGPlayerState(const FObjectInitializer& ObjectInitializer);

	// Player Information
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = "Player Info")
		FString PlayersName;
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = "Player Info")
		bool IsListenServer;

	UPROPERTY( EditDefaultsOnly, BlueprintReadOnly, Category = "Player Hand" )
		int32 HandMaxSize;

	// Turn
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = "Player Stats")
		bool IsActivePlayer;

	// Stats
	UPROPERTY( ReplicatedUsing=OnRep_Gold, VisibleAnywhere, BlueprintReadWrite, Category = "Player Stats")
		int32 Gold;

	UPROPERTY( ReplicatedUsing=AbilityMaskUpdated, VisibleAnywhere, BlueprintReadOnly, Category = "Player Stats" )
		FAbilityMask CurrentMask;

	UFUNCTION( BlueprintImplementableEvent, Category = "Player Stats" )
		void AbilityMaskUpdated();

	// Slots
	UPROPERTY(Replicated, ReplicatedUsing = OnRep_Weapon, EditAnywhere, BlueprintReadWrite, Category = "Player Hand")
		FCard Weapon;
	UPROPERTY( Replicated, ReplicatedUsing = OnRep_Equipment, EditAnywhere, BlueprintReadWrite, Category = "Player Hand" )
		FCard Equipment;

	UFUNCTION(BlueprintImplementableEvent, Category = "Player Hand")
		void OnRep_Weapon();
	UFUNCTION(BlueprintImplementableEvent, Category = "Player Hand")
		void OnRep_Equipment();
	
	// Hand
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Player Hand")
		TArray<FCard> HandCards;

	UPROPERTY(ReplicatedUsing = OnRep_Hand, VisibleAnywhere, BlueprintReadWrite, Category = "Player Hand")
		int32 CardCount;

	// Shop Selection
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Player Hand")
		TArray<FCard> ShopSelectionCards;

	UFUNCTION( BlueprintImplementableEvent, Category = "Player Hand" )
	void OnRep_Hand( int32 OldCounter );

	UFUNCTION( BlueprintImplementableEvent, Category = "Player Stats" )
	void OnRep_Gold();

	bool AddAction( FCard& SourceCard, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget );

	virtual void BeginPlay() override;

	void InitHand();
	void AddCardToHand(const FCard& Card);
	void RemoveCardFromHand(int32 HandIndex);
	void OrderCards(int32 Index);
	void EquipCard( const FCard& Card );

	int32 Attack( FMonster* Target, ACGPlayerState* Other );

	UFUNCTION( BlueprintCallable, Category = "Player Stats" )
	void PushUIAction( ECGAction::Type Type );

	UFUNCTION( Reliable, Server, WithValidation )
	void ServerPushUIAction( ECGAction::Type Type );

	//void CheckForMaskComplete();
	bool CheckAbilityMask( ECGAction::Type Type, const FCard& Card );
	bool FullAbilityCheck( ECGAction::Type Type, const FCard& Card, ACGPlayerState* Other );

	bool CheckActionCost( FCard& CardInfo );
	void ApplyActionCost( FCard& CardInfo );
	bool ActivateSlotAbility( ECGCard::Type SlotType );
	void Reset();

	UFUNCTION( BlueprintPure, Category = "Bit" )
	bool HasFlag( int32 Variable, EAbbilityTargets Flag )
	{
		return HAS_FLAG( Variable, Flag );
	}
	
	UFUNCTION( BlueprintCallable, Category = "Player Hand" )
	TArray<FCard>& GetSlotAB( ECGCard::Type Type );
	FCard& GetSlot( ECGCard::Type Type );

	UFUNCTION( BlueprintImplementableEvent, Category = "Player Hand" )
		void SlotActivated( ECGCard::Type Slot );
	UFUNCTION( Reliable, NetMulticast )
		void COM_SlotActived( ECGCard::Type Slot );

	void SelectSlot( ECGCard::Type Type, ACGPlayerState* Other );

	void UpdateSlot( ECGCard::Type Type );
	void ResetSlot( ECGCard::Type Type, bool Soft = true );
	bool SlotContainID( ECGCard::Type Type, int32 ID );
	FCard& GetCardFromSlot( ECGCard::Type Type, int32 ID );
	void RemoveIDFromSlot( ECGCard::Type Type, int32 ID );
	int32 CountIDInSlot( int32 ID, ECGCard::Type Slot );

	bool ApplyAbilityTURNEND( ACGPlayerState* OtherPlayer ); // before TURNSTART on other Player
	bool ApplyAbilityTURNSTART( ACGPlayerState* OtherPlayer ); // after reset
private:
	bool ApplyAbilityCOST( FCard& Source, bool bOnlyCheck = false ); // Return : bIsFree
	bool ApplyAbilityADD( FCard& Source, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget ); // Return : bCanBeAdd

	void EAddAction( FCard& Source, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget )
	{
		if( ExistingCard )
		{
			ExistingCard = false;
			AddAction( Source, MonsterTarget, PlayerTarget );
			ExistingCard = true;
		}
		else AddAction( Source, MonsterTarget, PlayerTarget );
	}

	int32 ApplyAbilitySELF( ECGCard::Type Type );
	int32 ApplyAbilityOPP( ECGCard::Type Type, ACGPlayerState* Other );
	bool ApplyAbilitySLOT( FCard& Source, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget );
	bool ApplyAbilityMONSTERDMG(); // Return : bInstandKill 
	bool ApplyAbilityLOOT( FLoot& Source, FMonster* MonsterTarget, ACGPlayerState* PlayerTarget ); // Return : bAbilityWasUsed
	bool ApplyAbilityDISC( FCard& Source ); 

	bool HasAbilityFlag( ECGCard::Type Type, EAbilityFlags flag ) const;
	void SetAbilityFlag( ECGCard::Type Type, EAbilityFlags flag );
	void RemoveAbilityFlag( ECGCard::Type Type, EAbilityFlags flag );

};
