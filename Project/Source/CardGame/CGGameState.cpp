#include "CardGame.h"
#include "CGGameState.h"

ACGGameState::ACGGameState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer) {

	CurrentPhase = ECGPhase::CGEquipPhase;
	GameIsRunning = false;
	DungeonLevel = 1;
	bReplicates = true;
	TurnTimeLimit = 100;
	bUseTurnTimer = false;

	PrimaryActorTick.bCanEverTick = true;
}

void ACGGameState::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	if(GameIsRunning) {
		CheckTurnTime(DeltaTime);
	}
}

void ACGGameState::StartGame(ACGPlayerState* PlayerState) {
	if( GameIsRunning )
	{
		UE_LOG(LogClass, Log, TEXT("*** Game is already running ***"));
		return;
	}
	if( PlayerArray.Num() > 1 )
	{
		LoadData();

		CurrentTimeLimit = TurnTimeLimit;

		ACGPlayerState* PlayerOne = (ACGPlayerState*)PlayerArray[0];
		ACGPlayerState* PlayerTwo = (ACGPlayerState*)PlayerArray[1];

		((ACGPlayerState*)PlayerArray[FMath::RandRange(0, 1)])->IsActivePlayer = true;
		PlayerOne->PlayersName = "Server";
		PlayerOne->IsListenServer = true;
		PlayerTwo->PlayersName = "Client";
		PlayerTwo->IsListenServer = false;

		ServerInitController();
		FConstPlayerControllerIterator Iter = GetWorld()->GetPlayerControllerIterator();
		Iter++;
		dynamic_cast<ACGPlayerController*>( &(**Iter) )->NetInit();

		GenerateStartHand();
		GenerateStartMonster();

		ElapsedTime = 0;
		GameIsRunning = true;
		GameHasStarted();
	}
}
// LATER MAYBE ON ANOTHER THREAD
void ACGGameState::LoadData() {

	// Check Monster Data
	if( MonsterDataTable == nullptr ) { UE_LOG(LogClass, Warning, TEXT("*** NO MONSTER DATA TABLE LINKED!!! ***")); return;	}
	// Check Loot Data
	if( LootDataTable == nullptr ) { UE_LOG( LogClass, Warning, TEXT( "*** NO LOOT DATA TABLE LINKED!!! ***" ) ); return; }
	// Check Gold Data
	if( GoldDataTable == nullptr ) { UE_LOG( LogClass, Warning, TEXT( "*** NO GOLD DATA TABLE LINKED!!! ***" ) ); return; }
	// Check Weapon & WeaponSlot Data
	if( WeaponDataTable == nullptr || WeaponSlotDataTable == nullptr ) { UE_LOG( LogClass, Warning, TEXT( "*** NO WEAPON DATA TABLE LINKED!!! ***" ) ); return; }
	// Check Equipment & EquipmentSlot Data
	if( EquipmentDataTable == nullptr || EquipmentSlotDataTable == nullptr ) { UE_LOG( LogClass, Warning, TEXT( "*** NO EQUIPMENT DATA TABLE LINKED!!! ***" ) ); return;	}
	// Check Ability Mask Data
	if( AbilityMasksTable == nullptr ) { UE_LOG( LogClass, Warning, TEXT( "*** NO LOOT DATA TABLE LINKED!!! ***" ) ); return; }

	for( auto it : MonsterDataTable->RowMap )		MonsterData.Add( *( reinterpret_cast<FMonsterData*>( it.Value ) ) );
	for( auto it : LootDataTable->RowMap )			LootData.Add( *( reinterpret_cast<FLootData*>( it.Value ) ) );
	for( auto it : GoldDataTable->RowMap )			GoldData.Add( *( reinterpret_cast<FGoldData*>( it.Value ) ) );
	for( auto it : WeaponDataTable->RowMap )		WeaponData.Add( *( reinterpret_cast<FCardData*>( it.Value ) ) );
	for( auto it : WeaponSlotDataTable->RowMap )	WeaponSlotData.Add( *( reinterpret_cast<FCardData*>( it.Value ) ) );
	for( auto it : EquipmentDataTable->RowMap )		EquipmentData.Add( *( reinterpret_cast<FCardData*>( it.Value ) ) );
	for( auto it : EquipmentSlotDataTable->RowMap ) EquipmentSlotData.Add( *( reinterpret_cast<FCardData*>( it.Value ) ) );
	for( auto it : AbilityMasksTable->RowMap )		AbilityMaskData.Add( *( reinterpret_cast<FAbilityMask*>( it.Value ) ) );

	auto SortMonster = []( const FMonsterData& Left, const FMonsterData& Right ) { return Left.Level < Right.Level; };
	auto SortLoot = []( const FLootData& Left, const FLootData& Right ) { return Left.Level < Right.Level; };
	auto SortGold = []( const FGoldData& Left, const FGoldData& Right ) { return Left.Level < Right.Level; };
	auto SortCard = []( const FCardData& Left, const FCardData& Right ) { return Left.Level < Right.Level; };
	auto SortMask = []( const FAbilityMask& Left, const FAbilityMask& Right ) { return Left.AbilityID < Right.AbilityID; };
	
	MonsterData.Sort( SortMonster );
	LootData.Sort( SortLoot );
	GoldData.Sort( SortGold );
	WeaponData.Sort( SortCard );
	WeaponSlotData.Sort( SortCard );
	EquipmentData.Sort( SortCard );
	EquipmentSlotData.Sort( SortCard );
	AbilityMaskData.Sort( SortMask );

	auto CountLevelEntries = []( uint32* DB, const TArray<FCardData>& Table )
	{
		for( const FCardData& CardEntry : Table )
		{
			DB[CardEntry.Level]++;
		}
	};
	
	CountLevelEntries( WeaponLevelCount, WeaponData );
	CountLevelEntries( WeaponSlotLevelCount, WeaponSlotData );
	CountLevelEntries( EquipmentLevelCount, EquipmentData );
	CountLevelEntries( EquipmentSlotLevelCount, EquipmentSlotData );
}

void ACGGameState::OnRep_GameIsRunning() {
	GameHasStarted();
}

void ACGGameState::GenerateStartMonster()
{
	for (int i = 0; i < 8; i++)
	{
		Monsters.Add( FMonster() );
		Monsters[i].GameState = this;
		Monsters[i].CreateMonsterCard(DungeonLevel);
		Monsters[i].Index = i;
		MonsterUpdate( i );
	}
}

void ACGGameState::GenerateStartHand() {
	ACGPlayerState* PlayerOne = (ACGPlayerState*)PlayerArray[0];
	ACGPlayerState* PlayerTwo = (ACGPlayerState*)PlayerArray[1];

	bool First = true;
	for( int32 i = 0; i < 3; ++i ) // Card each Type ( one round for slots )
	{
		for( ECGCard::Type j : { ECGCard::CGWeapon, ECGCard::CGEquipment } )
		{
			PlayerOne->AddCardToHand( GenerateCard( j, 0, First ) );
			PlayerTwo->AddCardToHand( GenerateCard( j, 0, First ) );
		}
		First = false;
	}
}

FCard ACGGameState::GenerateCard(ECGCard::Type Type, int32 Level, bool Slot /*= false */) {
	FCardData* DataPtr = nullptr;
	FCard NewCard;
	
	switch( Type ) 
	{
		case ECGCard::CGWeapon :
			DataPtr = ( Slot ) ? ( &WeaponSlotData[GetRandomIndex( WeaponSlotData, Level, reinterpret_cast<uint32*>( WeaponSlotLevelCount ) )] ) : ( &WeaponData[GetRandomIndex( WeaponData, Level, reinterpret_cast<uint32*>( WeaponLevelCount ) )] );
			break;
		case ECGCard::CGEquipment :
			DataPtr = ( Slot ) ? ( &EquipmentSlotData[GetRandomIndex( EquipmentSlotData, Level, reinterpret_cast<uint32*>( EquipmentSlotLevelCount ) )] ) : ( &EquipmentData[GetRandomIndex( EquipmentData, Level, reinterpret_cast<uint32*>( EquipmentLevelCount ) )] );
			break;
	}

	if( DataPtr != nullptr )
	{
		NewCard.CardType = Type;
		NewCard.Level = DataPtr->Level;
		NewCard.AbilityID = DataPtr->CardID;
		NewCard.Name = DataPtr->Name;
		NewCard.Discription = DataPtr->GetDescription( ECGRarity::CGBronze );
		NewCard.AP = DataPtr->APBronze;
		NewCard.AbilityID = DataPtr->EffectIDBronze;
		NewCard.Value = DataPtr->Value;
		NewCard.bIsSlot = Slot;
		NewCard.Param0 = DataPtr->ParamBronze0;
		NewCard.Texture = DataPtr->Texture;
	}
	
	return NewCard;
}


void ACGGameState::OnRep_Monster()
{
	// TODO : improve it!
	for( int32 i = 0; i < Monsters.Num(); ++i ) MonsterUpdate( i );
}

void ACGGameState::AddLog( bool Owning, ECGAction::Type Action, const FCard& Source, const FCard& Target, int32 Result, const FCard& Loot )
{
	FString FullSentence = ( Owning ) ? ( "You " ) : ( "Opponent " );

	switch( Action )
	{
	case ECGAction::CGPlay :
		FullSentence += "played " + Source.Name + " on the " + TypeToString( Target.CardType ) + " for " + FString::FromInt( Source.AP ) + " AP.";
		BattleLog_Updated( FullSentence );
		FullSentence = Source.Discription;
		break;
	
	case ECGAction::CGEquip :
		FullSentence += "equipped " + Source.Name;
		if( Source.AbilityID != -1 ) FullSentence += " : " + Source.Discription;
		break;
	
	case ECGAction::CGSelect :
		FullSentence += "selected ";
		if( Owning == ( Result > 0 ) ) FullSentence += "his ";
		else FullSentence += "your ";
		FullSentence += TypeToString( Source.CardType ) + " as target.";
		break;

	case ECGAction::CGAttack :
		if( Target.CardType == ECGCard::CGMonster )
		{
			FullSentence += "attacked a Monster with " + FString::FromInt( Source.Value ) + " DMG.";
		}
		else
		{
			FullSentence += "stole " + FString::FromInt( Result ) + " Gold from ";
			if( Owning ) FullSentence += "him.";
			else FullSentence += "you.";
		}
		BattleLog_Updated( FullSentence );
		FullSentence = "Abilities : " + Source.Discription;
		if( Target.CardType == ECGCard::CGMonster )
		{
			BattleLog_Updated( FullSentence );
			if( Result > 0 ) // won fight
			{
				FullSentence = "Monster made " + FString::FromInt( Result ) + " Damage.";
			}
			else
			{
				FullSentence = "Monster knocked ";
				if( Owning ) FullSentence += "you ";
				else FullSentence += "him ";
				FullSentence += "down and tries to steal " + FString::FromInt( Result * -1 ) + " Gold";
			}
		}
		break;

	case ECGAction::CGDiscard :
		FullSentence += "discarded " + Source.Name + " : " + Source.Discription;
		break;
	}
	BattleLog_Updated( FullSentence );
}
// LOG FUNCTIONS

void ACGGameState::DrawCard( ACGPlayerState* PlayerState, ECGCard::Type Type )
{
	PlayerState->AddCardToHand( GenerateCard( Type, DungeonLevel, false ) );
}

void ACGGameState::CheckMonsterLevel() {
	int32 Level = 99999;
	for (int i = 0; i < Monsters.Num(); i++) {
		if (Monsters[i].CardInformations.Level < Level) {
			Level = Monsters[i].CardInformations.Level;
		}
	}
	if (Level != DungeonLevel) {
		DungeonLevel = Level;

		DungeonLevelChanged(DungeonLevel);
	}
}

void ACGGameState::EndPhase()
{
	ServerPhaseEnded(CurrentPhase);
	switch (CurrentPhase)
	{
	case (ECGPhase::CGEquipPhase) :
		CurrentPhase = ECGPhase::CGAttackPhase;
		ServerPhaseStarted(ECGPhase::CGAttackPhase);
		UE_LOG(LogClass, Log, TEXT("*** Entered Attack Phase ***"));
		break;

	case (ECGPhase::CGAttackPhase) :
		CurrentPhase = ECGPhase::CGShopPhase;
		ServerPhaseStarted(ECGPhase::CGShopPhase);
		UE_LOG(LogClass, Log, TEXT("*** Entered Shop Phase ***"));
		break;

	case (ECGPhase::CGShopPhase) :
		// Reset Turntime
		CurrentTimeLimit = TurnTimeLimit;

		// Change Turnorder
		ACGPlayerState* P0 = dynamic_cast<ACGPlayerState*>( PlayerArray[0] );
		ACGPlayerState* P1 = dynamic_cast<ACGPlayerState*>( PlayerArray[1] );

		P0->IsActivePlayer = !P0->IsActivePlayer;
		P1->IsActivePlayer = !P1->IsActivePlayer;
		if( P1->IsActivePlayer )
		{
			P0->ApplyAbilityTURNEND( P1 );
			P1->Reset(); // Reset actionpoints
			P1->ApplyAbilityTURNSTART( P0 );
		}
		else
		{
			P1->ApplyAbilityTURNEND( P0 );
			P0->Reset();
			P0->ApplyAbilityTURNSTART( P1 );
		}

		// Change Phase
		CurrentPhase = ECGPhase::CGEquipPhase;
		ServerPhaseStarted(ECGPhase::CGEquipPhase); // Replication is not fast enough ( PhaseStarted called reach Client before IsActivePlayer value replication ) - Workaround, 0.2s Delay in BP
		
		
		UE_LOG(LogClass, Log, TEXT("*** Entered Equip Phase ***"));
		break;
	}
}

void ACGGameState::ServerPhaseEnded_Implementation( ECGPhase::Type Phase )
{
	PhaseEnded( Phase );
}

void ACGGameState::ServerPhaseStarted_Implementation( ECGPhase::Type Phase )
{
	PhaseStarted( Phase );
}

void ACGGameState::ServerInitController_Implementation()
{
	dynamic_cast<ACGPlayerController*>( GetWorld()->GetFirstPlayerController() )->NetInit();
}

void  ACGGameState::CheckTurnTime(float DeltaTime) {
	if (Role == ROLE_Authority && bUseTurnTimer ) 
	{
		CurrentTimeLimit -= DeltaTime;
		if (CurrentTimeLimit <= 0) {
			CurrentPhase = ECGPhase::CGShopPhase;
			EndPhase();
			UE_LOG(LogClass, Log, TEXT("*** Turntime over! ***"));
		}
		OnRep_CurrentTimeLimit();
	}
}

void ACGGameState::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME(ACGGameState, GameIsRunning);
	DOREPLIFETIME(ACGGameState, CurrentPhase);
	DOREPLIFETIME(ACGGameState, PlayersTurn);
	DOREPLIFETIME(ACGGameState, DungeonLevel);
	DOREPLIFETIME(ACGGameState, Monsters);
	DOREPLIFETIME(ACGGameState, LootData );
	DOREPLIFETIME(ACGGameState, MonsterData );
	DOREPLIFETIME(ACGGameState, GoldData );
	DOREPLIFETIME(ACGGameState, CurrentTimeLimit );
	DOREPLIFETIME( ACGGameState, AbilityMaskData );
}

int32 ACGGameState::GetRandomIndex( const TArray<FCardData>& DBType, int32 Level, const uint32* LevelCount ) const
{
	int32 EndIndex = -1;
	for( int32 i = 0; i <= Level; ++i )
	{
		EndIndex += LevelCount[i];
	}

	return FMath::RandRange( 0, EndIndex );
}

const FAbilityMask ACGGameState::GetAbilityMask( int32 AbilityID )
{
	if( AbilityID != -1 )
	{
		for( const FAbilityMask& AMask : AbilityMaskData )
		{
			if( AMask.AbilityID == AbilityID ) return AMask;
		}
	}
	return FAbilityMask();
}

const FAbilityMask ACGGameState::GetMaskEntry( int32 ID )
{
	for( const FAbilityMask& AMask : AbilityMaskData )
	{
		if( AMask.ID == ID ) return AMask;
	}
	return FAbilityMask();
}

void ACGGameState::OnRep_DungeonLevel()
{
	DungeonLevelChanged(DungeonLevel);
}